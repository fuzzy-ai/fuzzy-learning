# wuchen.coffee
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved.

_ = require 'lodash'
debug = require('debug')('learning:wu-chen')

FuzzyController = require '@fuzzy-ai/fuzzy-controller'
[eq, geq, leq] = require './equalish'

js = JSON.stringify

ALPHA = 0.5
EPSILON = 1e-7

makeDataTree = (rows, varNames, vars) ->

  debug "makeDataTree(rows, #{js varNames}, #{js vars})"

  node =
    defs: []
    subtrees: []

  head = vars[0]
  rest = vars.slice(1)

  j = varNames.indexOf(head)

  debug("j = #{j}")

  debug("rows before sort = #{js rows}")

  rows = _.sortBy rows, j

  debug("rows after sort = #{js rows}")

  col = _.map rows, j

  debug("col = #{js col}")

  if col.length > 3
    equivRel = equivalenceRelation col
    subsets = divideByAlphacuts equivRel, rows, ALPHA
  else
    subsets = [rows]

  debug "subsets = #{js subsets}"

  for subset, i in subsets
    subcol = _.map subset, j
    node.defs.push defFromSubcol subcol, ALPHA

  if rest? and rest.length > 0
    for subset, i in subsets
      node.subtrees.push makeDataTree subset, varNames, rest

  node

module.exports = wuchen = (rows, varNames, outputVarNames, codes) ->

  debug("wuchen(rows, #{js varNames}, #{js outputVarNames}, #{js codes})")

  inputVarNames = _.difference varNames, outputVarNames

  debug("inputVarNames = #{js inputVarNames}")

  result =
    inputs: {}
    outputs: {}

  for outputVarName, i in outputVarNames

    ls = _.concat [outputVarName], inputVarNames

    dataTree = makeDataTree rows, varNames, ls

    debug js(dataTree)

    model = modelFromDataTree dataTree, ls, i

    mergeModel result, model

  debug("result = #{js result}")

  fc = new FuzzyController result

  json = fc.toJSON()

  debug("json = #{js json}")

  omitted = _.omit(json, "parsed_rules")

  debug("omitted = #{js omitted}")

  omitted

equivalenceRelation = (arr) ->
  debug "column = #{js arr}"
  compat = distanceRelation arr
  debug "distance relation = #{js compat}"
  rel = closure compat
  debug "transitive closure = #{js rel}"
  rel

distanceRelation = (data) ->

  rel = new Array(data.length)

  for i in [0..rel.length - 1]
    rel[i] = new Array(rel.length)

  # XXX: pre-supposes dataset is sorted

  xm = data[data.length - 1]

  sum = 0

  for i in [0..data.length - 2]
    sum += Math.abs(xm - data[i])

  delta = sum / (data.length - 1)

  debug("delta = #{delta}")

  for i in [0..data.length - 1]
    for j in [0..data.length - 1]
      if eq(delta, 0)
        rel[i][j] = 1
      else
        rel[i][j] = Math.max(1 - Math.abs(data[i] - data[j]) / delta, 0)

  rel

closure = (data) ->

  T = Math.min

  res = new Array(data.length)

  for i in [0..res.length - 1]
    res[i] = new Array(res.length)
    for j in [0..res[i].length - 1]
      res[i][j] = data[i][j]

  for i in [0..res.length - 1]
    for j in [0..res.length - 1]
      for k in [0..res.length - 1]
        res[j][k] = Math.max(res[j][k], T(res[j][i], res[i][k]))

  res

divideByAlphacuts = (equivRel, rows, alpha) ->

  debug("divideByAlphacuts(equivRel, #{js rows}, #{js alpha})")

  sets = [[0]]
  n = 0
  i = 1

  while i < rows.length
    # XXX: Compare to first? Average? Central? Last?
    j = sets[n][0]
    equiv = equivRel[i][j]
    debug("equivRel[#{i}][#{j}] = #{equiv}")
    if equiv > alpha
      sets[n].push i
    else
      sets.push [i]
      n += 1
    i += 1

  debug("sets = #{js sets}")

  members = _.map sets, (set) -> _.map set, (j) -> rows[j]

  members

defFromSubcol = (subcol, alpha) ->

  min = subcol[0]
  max = subcol[subcol.length - 1]
  mid = (max + min) / 2

  if eq(min, max)
    left = mid - EPSILON
    right = mid + EPSILON
  else
    left = mid - (mid - min) / (1 - alpha)
    right = mid + (max - mid) / (1 - alpha)

  [left, mid, right]

modelFromDataTree = (dataTree, ls, i) ->

  debug("modelFromDataTree(#{js dataTree}, #{js ls}, #{js i})")

  model =
    inputs: {}
    outputs: {}
    parsed_rules: []

  head = ls[0]
  tail = ls.slice(1)

  model.outputs[head] = {}

  for def, j in dataTree.defs

    name = "set #{j}"

    model.outputs[head][name] = def

    debug("subtrees = #{js dataTree.subtrees[j]}")

    [defs, ants] = defsAndAntsFromSubtree dataTree.subtrees[j], tail, [i, j]

    _.defaultsDeep model.inputs, defs

    cons = {type: "is", dimension: head, set: name}

    for ant in ants
      rule =
        type: "if-then"
        antecedent: ant
        consequent: cons
        weight: 0.5

      model.parsed_rules.push rule

  model

defsAndAntsFromSubtree = (subtree, ls, path) ->

  debug("defsAndAntsFromSubtree(#{js subtree}, #{js ls}, #{js path})")

  head = ls[0]
  tail = ls.slice(1)

  defs = {}
  defs[head] = {}

  ants = []

  for def, i in subtree.defs

    full = _.concat(path, [i])
    name = "set #{full.join(' ')}"

    defs[head][name] = def

    if subtree.subtrees.length is 0
      ant =
        type: "is"
        dimension: head
        set: name
      debug("ant = #{js ant}")
      ants.push ant
    else
      sti = subtree.subtrees[i]
      [subdefs, subants] = defsAndAntsFromSubtree sti, tail, full

      _.defaultsDeep defs, subdefs

      for subant in subants
        ant =
          type: "and"
          left:
            type: "is"
            dimension: head
            set: name
          right: subant
        debug("ant = #{js ant}")
        ants.push ant

  debug("[defs, ants] = #{js [defs, ants]}")

  [defs, ants]

mergeModel = (left, right) ->

  _.defaultsDeep left.inputs, right.inputs
  _.defaultsDeep left.outputs, right.outputs

  left.parsed_rules = _.union(left.parsed_rules, right.parsed_rules)

  left
