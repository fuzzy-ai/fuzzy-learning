# compound.coffee
# Controller builder that uses a set builder and a rule builder
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>

FuzzyController = require '@fuzzy-ai/fuzzy-controller'
ControllerBuilder = require '../controller-builder'
RuleBuilder = require '../rule-builder'
SetBuilder = require '../set-builder'

class CompoundControllerBuilder extends ControllerBuilder

  constructor: (props) ->
    super(props)
    @setBuilder = SetBuilder.load @props.setBuilderType
    @ruleBuilder = RuleBuilder.load @props.ruleBuilderType
    @dataset = @props.dataset

  build: (dataset) ->
    if !dataset?
      dataset = @dataset
    sets = @setBuilder.buildSets dataset
    rules = @ruleBuilder.buildRules dataset, sets
    new FuzzyController sets.inputs, sets.outputs, rules

module.exports = CompoundControllerBuilder
