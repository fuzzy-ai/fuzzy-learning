# mlfe.coffee
# Controller builder using modified learn-from-example from Passarino et. al.
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>

_ = require 'lodash'
debug = require('debug')('learning:mlfe')

FuzzyController = require '@fuzzy-ai/fuzzy-controller'
ControllerBuilder = require '../controller-builder'

[eq, geq, leq] = require '../equalish'
maxError = require '../max-error'

TOLERANCE = 0.10
MEMBERSHIP_THRESHOLD = 0.90
js = JSON.stringify

class MLFEControllerBuilder extends ControllerBuilder

  constructor: (props) ->
    super(props)
    @tolerance = @props?.tolerance or TOLERANCE
    @threshold = @props?.threshold or MEMBERSHIP_THRESHOLD
    @dataset = @props.dataset

  build: (dataset) ->
    if !dataset?
      dataset = @dataset

    debug "Converting first row to data"

    {input, output} = dataset.row 0

    debug "inputs = #{js input}"
    debug "outputs = #{js output}"

    def = @initialRule input, output

    debug "Initial rule = #{js def}"

    fc = new FuzzyController def

    debug "fc = #{js fc}"

    expectedKeys = _.keys(fc.toJSON().inputs)

    for i in [1..dataset.rows.length - 1]

      {input, output} = dataset.row i

      debug "inputs = #{js input}"
      debug "outputs = #{js output}"

      debug "fc = #{js fc, "\n"}"

      providedKeys = _.keys(@withoutNulls(input))
      argKeys = _.intersection(expectedKeys, providedKeys)

      debug "argKeys = #{js argKeys}"

      inputArgs = _.pick(input, argKeys)

      debug "inputArgs = #{js inputArgs}"
      debug "_.keys(inputArgs) = #{js _.keys(inputArgs)}"

      actual = fc.evaluate inputArgs

      debug "actual = #{js actual}"

      me = maxError actual, output

      debug "max error = #{me}"

      if me > TOLERANCE
        debug "me #{me} > TOLERANCE #{TOLERANCE}; adding new rule"
        # XXX: use fc.addRule instead
        def = @addRule fc.toJSON(), input, output
        debug "Added rule"
        fc = new FuzzyController def

    fc

  withoutNulls: (obj) ->
    cp = {}
    for name, value of obj
      if value?
        cp[name] = value
    cp

  oom = (value) ->
    Math.pow(10, Math.round(Math.log10(value)))

  widthFrom = (value) ->
    width = oom(value) / 2
    if width is 0
      width = 0.5
    width

  initialRule: (inputs, outputs) ->

    def = {}
    def.inputs = _.mapValues @withoutNulls(inputs), (value) ->
      width = widthFrom value
      "set 0": [value - width, value, value + width]
    def.outputs = _.mapValues @withoutNulls(outputs), (value) ->
      width = widthFrom value
      "set 0": [value - width, value, value + width]

    andExp = (acc, value, name) ->
      if acc?
        type: "and"
        left:
          type: "is"
          dimension: name
          set: "set 0"
        right: acc
      else
        type: "is"
        dimension: name
        set: "set 0"

    antecedent = _.reduce @withoutNulls(inputs), andExp, null

    def.parsed_rules = _.map @withoutNulls(outputs), (value, name) ->
      type: "if-then"
      antecedent: antecedent
      consequent:
        type: "is"
        dimension: name
        set: "set 0"
      weight: 0.5

    def

  ensureSet = (value, sets) ->

    debug "Fuzzifying values"

    fuzzified = FuzzyController.fuzzify value, sets

    debug "fuzzified = #{js fuzzified}"

    setName = _.findKey fuzzified, (val) -> val >= MEMBERSHIP_THRESHOLD

    if setName
      debug "Found matching set #{setName} (#{fuzzified[setName]})"
    else
      debug "No matching set; creating one"
      n = _.size(sets)
      setName = "set #{n}"
      lower = upper = null
      for otherName, set of sets
        if set[1] < value and (!lower? or set[1] > sets[lower][1])
          lower = otherName
        else if set[1] > value and (!upper? or set[1] < sets[upper][1])
          upper = otherName
      sets[setName] = []
      sets[setName][1] = value
      width = widthFrom value
      if lower
        sets[setName][0] = sets[lower][1]
        sets[lower][2] = value
      else
        sets[setName][0] = value - width
      if upper
        sets[setName][2] = sets[upper][1]
        sets[upper][0] = value
      else
        sets[setName][2] = value + width

    setName

  addRule: (def, inputs, outputs) ->

    setNames = {}

    for name, value of @withoutNulls(inputs)
      if !def.inputs[name]
        def.inputs[name] = {}
      setNames[name] = ensureSet value, def.inputs[name]

    for name, value of @withoutNulls(outputs)
      if !def.outputs[name]
        def.outputs[name] = {}
      setNames[name] = ensureSet value, def.outputs[name]

    debug JSON.stringify(setNames)

    andExp = (acc, value, name) ->
      if acc?
        type: "and"
        left:
          type: "is"
          dimension: name
          set: setNames[name]
        right: acc
      else
        type: "is"
        dimension: name
        set: setNames[name]

    antecedent = _.reduce @withoutNulls(inputs), andExp, null

    parsed_rules = _.map @withoutNulls(outputs), (value, name) ->
      type: "if-then"
      antecedent: antecedent
      consequent:
        type: "is"
        dimension: name
        set: setNames[name]
      weight: 0.5

    def.parsed_rules = _.concat(def.parsed_rules, parsed_rules)

    debug JSON.stringify def

    _.omit def, "rules"

module.exports = MLFEControllerBuilder
