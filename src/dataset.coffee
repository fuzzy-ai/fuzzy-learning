# dataset.coffee
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

assert = require 'assert'

_ = require 'lodash'
debug = require('debug')('learning:dataset')

js = JSON.stringify

arrayOfStrings = (arr) ->
  _.isArray(arr) and _.every(outputColNames, _.isString)

class Dataset

  @fromCSV: (csv, outputColNames) ->

    assert _.isString(csv), "CSV must be a string"
    assert _.isUndefined(outputColNames) or arrayOfStrings(outputColNames)

    lines = csv.split("\n")
    colNames = lines.shift().split(',')

    if !outputColNames?
      outputColNames = [colNames[colNames.length - 1]]

    rows = lines.map (line) -> line.split(",").map (cell) -> parseFloat(cell)

    new Dataset rows, colNames, outputColNames

  constructor: (@rows, @colNames, @outputColNames) ->

    assert _.isArray(@rows), "@rows is not an array"
    for i in [0..@rows.length - 1]
      assert _.isArray(@rows[i]), "@rows[#{i}] is not an array"
      for j in [0..@rows[i].length - 1]
        val = @rows[i][j]
        msg = "@rows[#{i}][#{j}] is not a number"
        assert _.isNumber(val) or _.isNull(val), msg
    assert _.isArray(@colNames), "@colNames is not an array"
    for i in [0..@colNames.length - 1]
      assert _.isString(@colNames[i]), "@colNames[#{i}] is not a string"
    assert _.isArray(@outputColNames), "@outputColNames is not an array"
    for i in [0..@outputColNames.length - 1]
      assert _.isString(@outputColNames[i]),
        "@outputColNames[#{i}] is not a string"

  row: (i) ->

    row = @rows[i]
    data = _.zipObject @colNames, row

    inputData = _.omit data, @outputColNames
    outputData = _.pick data, @outputColNames

    {data: data, input: inputData, output: outputData}

  getColumn: (colName) ->

    j = @colNames.indexOf colName

    if j is -1
      throw new Error("Unknown column name #{colName}")

    debug("j for #{colName} is #{j}")

    col = []

    for i in [0..@rows.length - 1]
      debug("@rows[i] is #{js @rows[i]}")
      debug("@rows[i][j] is #{js @rows[i][j]}")
      col.push @rows[i][j]

    col

  isOutputColumn: (colName) ->

    debug("isOutputColumn(#{colName})")
    j = @outputColNames.indexOf(colName)
    debug("index in outputColNames = #{j}")
    j != -1

  partition: (cnt) ->

    chunks = _.times cnt, -> new Array()

    for i in [0..@rows.length - 1]
      chunks[i % cnt].push @rows[i]

    _.map chunks, (chunk) => new Dataset chunk, @colNames, @outputColNames

module.exports = Dataset
