# Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.io>
#
# All rights reserved.

assert = require 'assert'

debug = require('debug')('learning:set-builder')
_ = require 'lodash'

class SetBuilder

  @load: (name, dataset) ->
    debug("Loading set builder #{name}")
    cls = require "./set-builder/#{name}"
    debug("Finished require for #{name}")
    debug("cls = #{JSON.stringify cls}")
    assert _.isFunction(cls), "Could not load class for '#{name}'"
    return new cls(dataset)

  constructor: (@dataset) ->

  buildSets: (dataset) ->
    sets =
      inputs: {}
      outputs: {}
    for colName, i in dataset.colNames
      col = dataset.getColumn colName
      columnSets = @buildSetsForColumn col
      if dataset.isOutputColumn colName
        sets.outputs[colName] = columnSets
      else
        sets.inputs[colName] = columnSets
    sets

  buildSetsForColumn: (column) ->
    throw new Error("buildSetForColumn() not implemented")

module.exports = SetBuilder
