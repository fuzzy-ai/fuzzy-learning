# compound.coffee
# Compound indirect trainer
# Copyright 2017 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

assert = require 'assert'

_ = require 'lodash'
debug = require('debug')('learning:indirect-trainer:compound')

ControllerBuilder = require '../controller-builder'
Trainer = require '../trainer'
IndirectTrainer = require '../indirect-trainer'
Dataset = require '../dataset'

js = JSON.stringify

class CompoundIndirectTrainer extends IndirectTrainer

  constructor: (props) ->

    super props

    debug require('util').inspect @props

    @builder = @dynaload ControllerBuilder, @props.builderType
    @trainer = @dynaload Trainer, @props.trainerType
    @tolerance = @props.tolerance || 0.25

  dynaload: (type, arg) ->

    debug "Starting dynaload()"
    debug require('util').inspect type
    debug require('util').inspect arg

    if _.isString arg
      name = arg
    else if _.isObject arg
      {name, props} = arg
    else
      throw new Error("Unexpected argument to dynaload: #{arg}")

    if !props?
      props = {}

    assert _.isString name
    assert _.isObject props

    type.load name, props

  train: (fc, row, feedback) ->

    debug "train(#{js fc}, #{js row}, #{js feedback})"

    # A row mapping inputs and outputs to an error output

    errRow = []

    for key in _.keys(row.data).sort()
      errRow.push row.data[key]
    for key in _.keys(feedback).sort()
      errRow.push feedback[key]

    # Initial element of the dataset, or additional element

    if !@errDataset?
      colNames = []
      outputColNames = []
      for key in _.keys(row.data).sort()
        colNames.push key
      for key in _.keys(feedback).sort()
        colNames.push key
        outputColNames.push key
      @errDataset = new Dataset [errRow], colNames, outputColNames
    else
      @errDataset.addRow errRow

    if !@errController?
      @errController = @builder.build @errDataset
    else
      actual = @errController.evaluate row.data
      kit = (key) => @inTolerance feedback[key], actual[key]
      if !_.every(_.keys(feedback), (key) -> actual[key]?)
        @errController = @builder.build @errDataset
      else if !_.every(_.keys(feedback), kit)
        @trainer.train @errController, errRow
      else
        @trainer.train fc, row, new FuzzyErrorFunction(@errController)

module.exports = CompoundIndirectTrainer
