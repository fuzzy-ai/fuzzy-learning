# chinese-menu.coffee

_ = require 'lodash'

# One from column A, one from column B...

chineseMenu = (columns) ->
  mealsOf = (cols) ->
    meals = []
    if cols.length == 0
      return [{}]
    else
      [colName, dishes] = cols[0]
      restMeals = mealsOf cols.slice 1
      for dishName, price of dishes
        for restMeal in restMeals
          dish = {}
          dish[dishName] = price
          partial = {}
          partial[colName] = dish
          _.extend partial, restMeal
          meals.push partial
      # also, with no choice for this column
      for restMeal in restMeals
        partial = {}
        _.extend partial, restMeal
        meals.push partial
    meals
  mealsOf _.toPairs columns

module.exports = chineseMenu
