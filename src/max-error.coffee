# max-error.coffee

_ = require 'lodash'
debug = require('debug')('learning:max-error')

js = JSON.stringify

maxError = (actual, expected) ->

  debug "actual = #{js actual}"
  debug "expected = #{js expected}"

  error = _.mapValues expected, (value, name) ->
    if !actual[name]? and !value?
      0
    else if !actual[name]? or !value?
      Infinity
    else
      Math.abs((actual[name] - value) / value)

  debug "Error: #{js error}"

  _.max(_.values(error))

module.exports = maxError
