# gradient-descent.coffee -- GradientDescentTrainer
# Copyright 2014,2016 Fuzzy.ai <legal@fuzzy.io>
# All rights reserved.

assert = require 'assert'
_ = require 'lodash'
debug = require('debug')('learning:gradient-descent')

Trainer = require '../trainer'
[eq, geq, leq] = require '../equalish'
FuzzyController = require '@fuzzy-ai/fuzzy-controller'
DefaultErrorFunction = require '../error-function/default'

ALPHA0 = 10
TAU = 0.1
CEE = 0.1

ITERATIONS = 1

js = JSON.stringify

vectorSize = (vec) ->
  Math.sqrt(vec.reduce(((acc, n) -> acc + n * n), 0))

class GradientDescentTrainer extends Trainer

  trainToDataset: (fc, dataset) ->
    debug("Training to dataset")
    debug("Training with #{ITERATIONS} iterations")
    for i in [0..ITERATIONS - 1]
      debug("Beginning iteration #{i}")
      for j in [0..dataset.rows.length - 1]
        debug("Row #{j} for iteration #{i}")
        row = dataset.row j
        @train fc, row
    fc

  train: (fc, row, efunc) ->

    if !efunc?
      efunc = new DefaultErrorFunction({expected: row.output})

    assert _.isObject(fc), "fc argument is not an object"
    for method in ["evaluate", "toArray", "toJSON", "slopeAt", "fromArray"]
      assert _.isFunction(fc[method]), "fc has no #{method}() method"
    assert _.isObject(row), "row is not an argument"
    assert _.isObject(row.input), "row does not have input"
    assert _.isObject(row.output), "row does not have output"
    assert _.isObject(efunc), "efunc argument is not an object"
    for method in ["err", "slope"]
      assert _.isFunction(efunc[method]), "efunc has no #{method}() method"

    debug("train(fc, #{js row})")

    {input, output} = row

    actual = fc.evaluate input

    debug("actual = #{js actual}")

    if _.every(actual, _.isNull)
      debug "No actual results; can't train"
      return

    err = efunc.err(row.input, actual)
    debug "err = #{js err}"

    dup = new FuzzyController _.omit(fc.toJSON(), ["rules"])

    theta = fc.toArray()

    debug("theta = #{js theta}")

    outputNames = _.keys(output)

    tmp = _.map theta, (b, i) ->
      fc.slopeAt(i, b, input)

    debug("tmp = #{js tmp}")

    assert _.every(tmp, _.isObject), "tmp has non-object values: #{js tmp}"
    assert _.every(tmp, (obj) -> _.every(obj, _.isFinite)),
      "tmp has non-numeric values: #{js tmp}"

    slope = {}
    for name in outputNames
      slope[name] = _.map(tmp, (sa) -> sa[name])

    debug("slope = #{js slope}")

    eslope = efunc.slope(row.input, actual)
    debug "eslope = #{js eslope}"

    p = {}
    m = {}

    for name in outputNames
      # gradient in error space, using the chain rule
      grad = _.map slope[name], (val) -> eslope[name] * val
      debug("grad = #{grad} for #{name}")
      # p = unit vector to move in - descending the gradient
      # THUS THE NAME
      sgrad = vectorSize(grad)
      debug("sgrad = #{sgrad} for #{name}")
      p[name] = _.map grad, (val) -> -1.0 * val / sgrad
      # Supposed to be a unit vector fool
      psize = vectorSize(p[name])
      assert eq(psize, 1.0), "|p[#{name}]| != 1.0: #{psize}"
      m[name] = p[name].reduce(((acc, val, i) -> acc + val * grad[i]), 0)

    debug("grad = #{js grad}")

    debug("p = #{js p}")

    debug("m = #{js m}")

    for name, value of m
      assert.ok value < 0, "m is not less than zero for #{name} (#{value})"

    alpha = ALPHA0
    n = 0

    # We try to identify a value of alpha that moves us about the
    # right distance using backtracking line search
    #
    # https://en.wikipedia.org/wiki/Backtracking_line_search

    while true
      debug("iter #{n}; alpha = #{alpha}")
      delta = {}
      for name in _.keys(output)
        delta[name] = _.map theta, (b, i) ->
          if _.isNull(p[name][i])
            0.0
          else
            alpha * p[name][i]

      debug("delta = #{js delta}")
      for name, values of delta
        debug("|delta[#{name}]| = #{vectorSize(values)}")

      next = _.clone(theta)
      for i in [0..theta.length - 1]
        for name in outputNames
          next[i] += delta[name][i]

      debug("next = #{js next}")

      dup.fromArray next

      nextActual = dup.evaluate input

      debug("nextActual = #{js nextActual}")

      allOK = true

      nextErr = efunc.err(row.input, nextActual)
      debug "nextErr = #{js nextErr}"
      debug "err = #{js err}"

      for name in outputNames
        debug "testing for #{name}"
        expected = alpha * CEE * m[name]
        debug "expected = #{expected}"
        if (nextErr[name] > err[name] + expected)
          debug "Not OK; breaking"
          allOK = false
          break
      if allOK
        debug "All is OK; breaking with update"
        fc.fromArray dup.toArray()
        break
      else if n >= 50
        debug "Too many iters; breaking without update"
        throw new Error("Too many iters: #{n}")
        break
      else
        debug "Not successful; iterating"
        alpha *= TAU
        n++

module.exports = GradientDescentTrainer
