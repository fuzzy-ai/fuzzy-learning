# Copyright 2014 9165584 Canada Corporation <legal@fuzzy.io>
#
# All rights reserved.

_ = require 'lodash'
Matrix = require 'matrixmath/Matrix'

Trainer = require './trainer'

class NewtonsMethodTrainer extends Trainer

  constructor: (fc, data) ->
    super(fc, data)
    @previous = []
    for row in @data
      @train row

  train: (row) ->
    {inputs, outputs} = row
    @last = @fc.toArray()
    theta = @last
    slope = _.map(theta,  (b, i) => @fc.slopeAt(i, b, inputs))
    actual = @fc.evaluate inputs
    seconds = _.map theta,  (b, i) =>
      _.map theta, (c, j) =>
        @fc.secondDerivative(i, j, b, c, inputs)
    for name in _.keys(outputs)
      m1 = new Matrix(theta.length, 1)
      m1.setData _.map(slope, (obj) -> obj[name])
      console.log m1.toLogString()
      m2 = new Matrix theta.length, theta.length
      m2.setData _.map(_.flatten(seconds), (obj) -> obj[name])
      console.log m2.toLogString()
      m3 = m2.invert()
      console.log m3.toLogString()
      m4 = Matrix.multiply(m3, m2)
      console.log(m4.toLogString())

  stepSize: (k) ->
    # 0.1 * Math.exp(-0.01*@k)
    0.01

module.exports = NewtonsMethodTrainer
