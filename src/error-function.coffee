# error-function.coffee
# superclass for error functions
# Copyright 2017 Fuzzy.ai <legal@fuzzy.ai>

class ErrorFunction

  @load: (name, props) ->
    cls = require "./error-function/#{name}"
    return new cls(props)

  constructor: (@props) ->

  err: (inputs, outputs) ->
    null

  slope: (inputs, outputs) ->
    null

module.exports = ErrorFunction
