# mean-squared-error.coffee -- Tester by mean squared error
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved.

_ = require "lodash"

Tester = require "../tester"

class MeanSquaredErrorTester extends Tester

  test: (fc, dataset) ->
    se = {}
    for name in dataset.outputColNames
      se[name] = new Array(dataset.rows.length)
    for i in [0..dataset.rows.length - 1]
      row = dataset.row i
      actual = fc.evaluate row.input
      for name in dataset.outputColNames
        se[name].push Math.pow(actual[name] - row.output[name], 2)
    _.mapValues se, _.mean

module.exports = MeanSquaredErrorTester
