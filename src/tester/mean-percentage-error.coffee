# mean-percentage-error.coffee -- Tester by mean percentage error
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved.

_ = require "lodash"

debug = require('debug')('learning:mean-percentage-error')

Tester = require "../tester"
[eq, geq, leq] = require "../equalish"

class MeanPercentageErrorTester extends Tester

  test: (fc, dataset) ->
    se = {}
    for name in dataset.outputColNames
      se[name] = new Array()
    for i in [0..dataset.rows.length - 1]
      row = dataset.row i
      actual = fc.evaluate row.input
      for name in dataset.outputColNames
        # XXX: Skipping zeroes
        if !eq(row.output[name], 0)
          pe = Math.abs((actual[name] - row.output[name]) / row.output[name])
          se[name].push pe

    debug JSON.stringify(se)
    _.mapValues se, _.mean

module.exports = MeanPercentageErrorTester
