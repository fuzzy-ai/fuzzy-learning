# error-function.coffee -- ErrorFunctionTester class
# Copyright 2017 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved.

_ = require 'lodash'
debug = require('debug')('learning:error-function-tester')

Tester = require '../tester'
ErrorFunction = require '../error-function'

class ErrorFunctionTester extends Tester
  constructor: (props) ->
    super(props)
    @efuncs = {}
    for name, value of @props
      if value instanceof ErrorFunction
        @efuncs[name] = value

  test: (fc, dataset) ->
    se = {}
    for name in _.keys(@efuncs)
      se[name] = new Array()
    for i in [0..dataset.rows.length - 1]
      row = dataset.row i
      actual = fc.evaluate row.input
      for name in _.keys(@efuncs)
        se[name].push @efuncs[name].err row.input, actual
    debug se
    _.mapValues se, _.mean

module.exports = ErrorFunctionTester
