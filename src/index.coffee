# index.coffee
# Main module for fuzzy-learning
# Copyright 2017 Fuzzy.ai
# All rights reserved

module.exports =
  ControllerBuilder: require('./controller-builder')
  Dataset: require('./dataset')
  RuleBuilder: require('./rule-builder')
  SetBuilder: require('./set-builder')
  Tester: require('./tester')
  Trainer: require('./trainer')
