# genetic.coffee

assert = require 'assert'
util = require 'util'

_ = require 'lodash'
debug = require('debug')('learning:genetic')

FuzzyController = require '@fuzzy-ai/fuzzy-controller'

cluster = require './cluster'
[eq, geq, leq] = require './equalish'
chineseMenu = require '../chinese-menu'

TOLERANCE = 0.01
MAX_POPULATION = 100
SURVIVAL_RATE = 0.25
DEFAULT_WEIGHT = 0.5
MUTATION_RATE = 0.50
MUTATION_RANGE = 0.25
DATA_SAMPLE_RATE = 0.01

aavg = (arr) ->
  if arr.length == 0
    null
  else
    _.reduce(arr, (a, x) -> a + x) / arr.length

median = (arr) ->
  if arr.length % 2 == 0
    aavg [arr[arr.length / 2 - 1], arr[arr.length / 2]]
  else
    arr[Math.ceil(arr.length / 2.0)]

js = JSON.stringify

module.exports = genetic = (rows, varNames, outputVarNames) ->

  debug("varNames = #{js varNames}")
  debug("outputVarNames = #{js outputVarNames}")
  debug("Creating prototype")

  proto = prototype rows, varNames, outputVarNames

  debug("proto = #{js proto}")

  debug("Creating initial population")

  population = initialize proto

  debug("population.length = #{population.length}")

  data = _.map rows, (row) -> _.zipObject varNames, row

  generation = 0

  while true

    debug("Beginning generation: #{generation}")

    debug("Evaluating performance for generation: #{generation}")

    performance = evaluate population, data, proto

    avg = aavg performance

    debug("Average performance for generation #{generation} is #{avg}")
    debug("Comparing performances for generation #{generation}")

    {best, bestModel} = compare population, performance

    debug("Best performance for generation #{generation} is #{best}")
    debug("Best model for generation #{generation} is #{js bestModel}")

    if best < TOLERANCE

      debug("Performance goal of #{TOLERANCE} reached at gen. #{generation}")

      # XXX: Test will full data set

      break

    else

      debug("Performance goal of #{TOLERANCE} not reached")
      debug("Breeding next generation for #{generation}")

      population = breed population, performance

      generation++

  debug "Best model = #{js bestModel}"

  finalFC = new FuzzyController proto
  finalFC.fromArray bestModel
  finalFC.toJSON()

prototype = (rows, varNames, outputVarNames) ->
  {inputs, outputs} = cluster rows, varNames, outputVarNames
  rules = defaultRules inputs, outputs

  fc = new FuzzyController
    inputs: inputs
    outputs: outputs
    rules: rules

  _.omit fc.toJSON(), ["rules"]

initialize = (proto) ->

  population = []
  fc = new FuzzyController proto
  base = fc.toArray()

  _.times MAX_POPULATION, (i) ->
    model = base.slice()
    randomWeights model, proto.parsed_rules.length
    maybeMutate model
    population.push model

  population

mutate = (model) ->
  i = _.random 0, model.length - 1
  model[i] = (model[i] - MUTATION_RANGE) +
    (Math.random() * (model[i] * 2 * MUTATION_RANGE))
  model

maybeMutate = (model) ->

  if MUTATION_RATE > 1
    _.times Math.floor(MUTATION_RATE), (i) ->
      mutate model

  pr = MUTATION_RATE % Math.floor(MUTATION_RATE)

  if Math.random() < pr
    mutate model

  model

error = (actual, expected) ->
  Math.abs(actual - expected) / (Math.abs(actual) + Math.abs(expected))

evaluate = (population, data, proto) ->
  performance = []
  i = 0
  debug("Data size = #{data.length}")
  sampleCount = Math.round(data.length * DATA_SAMPLE_RATE)
  samples = _.sampleSize(data, sampleCount)
  debug("Sample size = #{samples.length}")
  fc = new FuzzyController proto
  for model in population
    debug("Evaluation of model #{i}")
    fc.fromArray model
    de = []
    for datum in samples
      inputs = _.pick datum, _.keys(proto.inputs)
      results = fc.evaluate inputs
      onames = _.keys(proto.outputs)
      errors = _.map onames, (name) -> error results[name], datum[name]
      avg = aavg errors
      de.push avg
    deavg = aavg de
    performance.push deavg
    i++
  assert.ok population.length == performance.length
  performance

compare = (population, performance) ->
  best = Infinity
  bestModel = null
  for model, i in population
    perf = performance[i]
    if perf < best
      best = perf
      bestModel = model
  {best: best, bestModel: bestModel}

breed = (population, performance) ->
  next = []
  survivors = survival population, performance
  _.times MAX_POPULATION, (i) ->
    # XXX: weighted random, based on performance?
    p1 = _.sample survivors
    p2 = _.sample survivors
    child = combine p1, p2
    maybeMutate child
    next.push child
  next

survival = (population, performance) ->
  recs = []
  for model, i in population
    recs.push {model: model, performance: performance[i]}
  recs = _.sortBy recs, "performance"
  max = Math.round(recs.length * SURVIVAL_RATE)
  aperf = performance.slice()
  aperf.sort()
  survivors = _.map(recs.slice(0, max), "model")
  survperf = _.map(recs.slice(0, max), "performance")
  debug "survivor count = #{survivors.length}"
  debug "best performance = #{survperf[0].toFixed(3)}"
  debug "median perf for survivors #{median(survperf).toFixed(3)}"
  debug "worst survivor performance #{survperf[survperf.length-1].toFixed(3)}"
  debug "median of all performance #{median(aperf).toFixed(3)}"
  debug "worst of all performance #{aperf[aperf.length - 1].toFixed(3)}"

  survivors

combine = (p1, p2) ->
  assert.equal p1.length, p2.length
  next = new Array(p1.length)
  for value, i in next
    if _.random(0, 1) is 0
      next[i] = p1[i]
    else
      next[i] = p2[i]
  next

toPhrase = (setValue, dim) ->
  [set] = _.keys setValue
  "#{dim} IS #{set}"

defaultRules = (inputs, outputs) ->
  rules = []
  ics = chineseMenu inputs
  ocs = chineseMenu outputs
  for ic in ics
    ant = _.map(ic, toPhrase).join(" AND ")
    for oc in ocs
      cons = _.map(oc, toPhrase).join(" AND ")
      rules.push "IF #{ant} THEN #{cons} WITH #{DEFAULT_WEIGHT}"
  rules

randomWeights = (model, ruleCount) ->
  start = model.length - 1 - ruleCount
  end = model.length - 1
  for i in [start..end]
    model[i] = Math.random()
  model
