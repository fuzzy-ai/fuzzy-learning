# lfe.coffee
# Learn-From-Example builder from Passino-Yurkovich
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved.

_ = require 'lodash'
debug = require('debug')('learning:lfe')

FuzzyController = require '@fuzzy-ai/fuzzy-controller'
[eq, geq, leq] = require '../equalish'
RuleBuilder = require '../rule-builder'

js = JSON.stringify

class LFERuleBuilder extends RuleBuilder

  buildRules: (dataset, sets) ->

    if !dataset?
      dataset = @dataset

    rows = dataset.rows
    varNames = dataset.colNames
    outputVarNames = dataset.outputColNames
    codes = dataset.codes

    debug "varNames = #{js varNames}"
    debug "outputVarNames = #{js outputVarNames}"
    debug "codes = #{js codes}"

    {inputs, outputs} = sets

    rules = []
    seenRules = {}

    for row in rows

      data = _.zipObject varNames, row

      iData = _.omit data, outputVarNames

      # Do separate rules for separate outputs
      for outputVarName in outputVarNames

        oData = _.pick data, [outputVarName]
        selected = _.pick outputs, [outputVarName]

        {degree, key, imem, omem} = toRule iData, oData, inputs, selected

        debug key

        if _.has seenRules, key
          if degree > seenRules[key][0]
            seenRules[key] = [degree, imem, omem]
        else
          seenRules[key] = [degree, imem, omem]

    seenRules = removeConstants seenRules

    rules = []

    for key, value of seenRules

      [degree, imem, omem] = value
      ant = toHC imem
      con = toHC omem

      rule = "IF #{ant} THEN #{con} WITH 0.5"

      rules.push rule

    rules = _.uniq rules

    rules.sort()

    rules

mems = (data, sets) ->
  mem = {}
  for name, datum of data
    shape = sets[name]
    fuzzified = FuzzyController.fuzzify datum, shape
    mem[name] = [null, Number.NEGATIVE_INFINITY]
    for setName, membership of fuzzified
      # XXX: This will arbitrarily favor earlier names
      if membership > mem[name][1]
        mem[name] = [setName, membership]

  # Filter to only keys with good values

  gk = _.filter _.keys(mem), (key) ->
    # There was some set membership and it was greater than zero
    !_.isNull(mem[key][0]) and !eq(mem[key][1], 0)

  # Pick only good keys

  mem = _.pick mem, gk

  mem

wrap = (str) ->
  if str.match /\s+/ then "[#{str}]" else str

toIs = (dimensionName, setName) ->
  d = wrap dimensionName
  s = wrap setName
  "#{d} IS #{s}"

toHC = (mem) ->
  strs = _.map mem, (value, key) -> toIs(key, value[0])
  str = strs.join(" AND ")
  str

toRule = (iData, oData, inputs, outputs) ->

  prod = (p, value) -> p * value[1]

  imem = mems iData, inputs
  omem = mems oData, outputs

  ant = toHC imem
  adeg = _.reduce _.values(imem), prod, 1

  con =  toHC omem
  cdeg = _.reduce _.values(omem), prod, 1

  key = "#{ant} -> #{con}"

  {key: key, imem: imem, omem: omem, degree: adeg * cdeg}

removeConstants = (seenRules) ->

  values = _.values seenRules

  if values.length < 1
    return seenRules

  # Constants will be in every value, including the first

  first = values[0]

  for dimensionName, pair of first[1]
    [setName, degree] = pair
    # It's universal if every rule has this input-set pair
    universal = _.every values, (ruleVal) ->
      ruleVal[1][dimensionName]? && ruleVal[1][dimensionName][0] == setName
    if universal
      for value in values
        delete values[1][dimensionName]

  sr = {}

  for value in values
    ant = toHC value[1]
    con = toHC value[2]
    key = "#{ant} -> #{con}"
    sr[key] = value

  sr

module.exports = LFERuleBuilder
