# flfe.coffee

_ = require 'lodash'
debug = require('debug')('learning:flfe')

FuzzyController = require '@fuzzy-ai/fuzzy-controller'
[eq, geq, leq] = require '../equalish'
maxError = require '../max-error'
RuleBuilder = require '../rule-builder'
chineseMenu = require '../chinese-menu'

TOLERANCE = 0.05
BASE = 2

js = JSON.stringify

class FLFERuleBuilder extends RuleBuilder

  buildRules: (dataset, sets) ->

    debug "Creating initial no-rules fuzzy controller"

    fc = new FuzzyController
      inputs: sets.inputs
      outputs: sets.outputs
      rules: []

    seen = {}

    debug "iterating through rows"

    for i in [0..dataset.rows.length - 1]

      {input, output} = dataset.row i

      debug "Training from row: #{js input} => #{js output}"

      if not @predicts fc, input, output

        rules = @makeRules fc, input, output, seen
        @addNewRules fc, rules

    debug "Returning fuzzy controller JSON"

    json = fc.toJSON()
    json.rules

  predicts: (fc, inputData, expected) ->

    debug "Evaluating #{js inputData}"

    actual = fc.evaluate inputData

    debug "Output: #{js actual}"
    debug "Calculating error"

    me = maxError actual, expected

    maxError < TOLERANCE

  toPhrase: (setValue, dim) =>
    [set] = _.keys setValue
    "#{@wrap dim} is #{@wrap set}"

  wrap: (str) ->
    if str.match /\W/
      "[#{str}]"
    else
      str

  makeRules: (fc, input, output, seen) ->

    json = fc.toJSON()

    orig = _.clone(json.rules)

    resp = (combo) ->
      ant = _.omit combo, outputVarNames
      Math.pow BASE, _.keys(ant).length - 1

    debug "Adding rules to fuzzy controller"

    outputVarNames = _.keys json.outputs
    inputCount = _.keys(json.inputs).length

    fuzzified = {}

    for name, datum of input
      fuzzified[name] = FuzzyController.fuzzify datum, json.inputs[name]

    for name, datum of output
      fuzzified[name] = FuzzyController.fuzzify datum, json.outputs[name]

    debug "Fuzzified = #{js fuzzified}"

    sets = {}

    for name, memberships of fuzzified
      abbrev = {}
      for setName, membership of memberships
        if membership > 0.0
          abbrev[setName] = membership
      sets[name] = abbrev

    debug "Sets = #{js sets}"

    combos = chineseMenu sets

    combos = _.filter combos, (combo) ->

      ant = _.omit combo, outputVarNames
      cons = _.pick combo, outputVarNames

      _.keys(ant).length > 0 and _.keys(cons).length > 0

    # We try to scale the weights of rules based on their
    # "responsibility" for the outcomes. This is based on the idea that more
    # specific antecedents are more responsible for the consequent.

    resps = _.map combos, (combo) -> resp combo
    totalResp = _.reduce resps, (acc, resp) -> acc + resp

    debug "Combos = #{js combos}"

    for combo in combos

      debug "combo = #{js combo}"

      ant = _.omit combo, outputVarNames
      cons = _.pick combo, outputVarNames

      debug "ant = #{js ant}, cons = #{js cons}"

      if _.keys(ant).length == 0
        debug "Aborting; no antecedent"
        continue

      if _.keys(cons).length == 0
        debug "Aborting; no consequent"
        continue

      antMem = andMemberships ant
      consMem = andMemberships cons

      debug "antMem = #{antMem}"
      debug "consMem = #{consMem}"

      rawWeight = consMem / antMem

      debug "rawWeight = #{rawWeight}"

      debug "resp = #{resp combo}"
      debug "totalResp = #{totalResp}"

      scale = resp(combo) / totalResp

      debug "scale = #{scale}"

      weight = rawWeight * scale

      debug "weight = #{weight}"

      weight = Math.min 1.0, Math.max weight, 0.0

      debug "weight = #{weight} after clipping"

      antPhrases = _.map ant, @toPhrase
      consPhrases = _.map cons, @toPhrase

      antPhrases.sort()
      consPhrases.sort()

      antecedent = antPhrases.join " AND "
      consequent = consPhrases.join " AND "

      ruleMain = "IF #{antecedent} THEN #{consequent}"

      debug "ruleMain = #{ruleMain}"

      existent = _.find json.rules, (item) ->
        item.match "^#{ruleMain}"

      debug "existent = #{existent}"

      if existent?
        _.remove json.rules, (str) -> str is existent
        rec = seen[ruleMain]
        if !rec?
          debug "*** NO SEEN RECORD FOR RULE ***"
          seen[ruleMain] = {count: 1, weight: weight}
        else
          debug "We've seen this before"
          debug "rec = #{js rec}"
          weight = (weight + (rec.weight * rec.count)) / (rec.count + 1 )
          seen[ruleMain] = {count: rec.count + 1, weight: weight}
      else
        debug "Adding rule"
        seen[ruleMain] = {count: 1, weight: weight}

      debug "new rec = #{js seen[ruleMain]}"

      rule = "#{ruleMain} WITH #{weight}"

      debug "rule = #{rule}"

      json.rules.push rule

    # Return the list of new rules

    _.difference json.rules, orig

  andMemberships = (mems) ->

    degrees = _.map mems, (setValue) ->
      _.values(setValue)[0]

    _.reduce degrees, (acc, x) -> x * acc

  addNewRules: (fc, rules) ->
    for rule in rules
      fc.addRule rule

module.exports = FLFERuleBuilder
