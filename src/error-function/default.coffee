# default.coffee
# Default error function for optimization
# Copyright 2017 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

assert = require 'assert'

_ = require 'lodash'
debug = require('debug')('learning:error-function:default')

ErrorFunction = require '../error-function'

class DefaultErrorFunction extends ErrorFunction

  constructor: (props) ->
    super(props)
    debug(@props)
    assert _.isObject(@props), "no argument for DefaultErrorFunction"
    assert _.isObject(@props.expected), "no expected for DefaultErrorFunction"
    @expected = @props.expected

  err: (inputs, actual) ->
    _.mapValues actual, (value, key) =>
      if @expected[key]?
        0.5 * Math.pow(value - @expected[key], 2)
      else
        null

  slope: (inputs, actual) ->
    _.mapValues actual, (value, key) =>
      if @expected[key]?
        value - @expected[key]
      else
        null

module.exports = DefaultErrorFunction
