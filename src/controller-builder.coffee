# ControllerBuilder 2014-2016 9165584 Canada Corporation <legal@fuzzy.io>
#
# All rights reserved.

class ControllerBuilder

  @load: (name, props) ->
    cls = require "./controller-builder/#{name}"
    return new cls(props)

  constructor: (@props) ->

  build: (dataset) ->
    throw new Error("build() unimplemented")

module.exports = ControllerBuilder
