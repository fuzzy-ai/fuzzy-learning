# partition.coffee
# Make sets based on discrete values
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

_ = require 'lodash'
debug = require('debug')('learning:partition')

SetBuilder = require '../set-builder'

EPSILON = 1e-6

js = JSON.stringify

class PartitionSetBuilder extends SetBuilder

  buildSetsForColumn: (column) ->

    values = _.uniq column

    sets = {}

    for value in values
      debug("value = #{value}")
      sets["code #{value}"] = [value - EPSILON, value, value + EPSILON]

    sets

module.exports = PartitionSetBuilder
