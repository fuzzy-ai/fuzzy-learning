# range.coffee
# Create evenly-spaced fuzzy sets in a range
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved.

_ = require 'lodash'
debug = require('debug')('learning:range')

SetBuilder = require '../set-builder'
SetNamer = require '../set-namer'
[eq, geq, leq] = require '../equalish'

DEFAULT_COUNT = 7
DEFAULT_RANGE = [0, 100]

debug "Finished requires"

class RangeSetBuilder extends SetBuilder

  buildSetsForColumn: (column) ->

    range = @rangeOf column

    debug range

    sets = @expandRangeToSets range

    sets

  rangeOf: (column) ->

    if _.isArray(column) and column.length > 0
      [_.min(column), _.max(column)]
    else
      DEFAULT_RANGE

  expandRangeToSets: (range, dimensionName) ->

    [low, high, count] = range
    sets = {}

    if !count?
      count = DEFAULT_COUNT

    if eq(low, high)

      # We create a singular value, but account for the
      # possibility of lower or higher values

      # Epsilon used in fuzzy-controller is 1e-8, so we try to beat that
      # by 2 OOM

      if eq(low, 0.0)
        epsilon = 1e-6
      else
        epsilon = low * 1e-6

      sets =
        low: [low - epsilon, low]
        medium: [low - epsilon, low, low + epsilon]
        high: [low, low + epsilon]

    else if eq(low, 0) and eq(high, 1)

      sets =
        false: [-1, 0, 1]
        true: [0, 1, 2]

    else

      names = SetNamer.nameSets count, dimensionName

      interval = (high - low) / (count - 1)

      for i in [0..count - 1]
        if i == 0 # left shoulder
          sets[names[i]] = [low, low + interval]
        else if i == count - 1  # left shoulder
          sets[names[i]] = [high - interval, high]
        else
          sets[names[i]] = [
            low + (i - 1) * interval
            low + i * interval
            low + (i + 1) * interval
          ]

    sets

debug "Finished class"

module.exports = RangeSetBuilder

debug module.exports
