# cluster.coffee
# Fuzzy c-means clustering algorithm
# Copyright 2015-2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved.

_ = require 'lodash'

SetBuilder = require '../set-builder'
SetNamer = require '../set-namer'

class ClusterSetBuilder extends SetBuilder

  buildSetsForColumn: (column) ->

    cl = cluster _.filter(column), 5, 0.01, 2
    cl

  initializeU = (I, J) ->
    u = []
    for i in [0..I - 1]
      u[i] = new Array(J)
      for j in [0..J - 1]
        u[i][j] = Math.random()
    u

  centre = (x, u, j, m) ->
    num = 0
    den = 0
    for i in [0..x.length - 1]
      num += x[i] * Math.pow(u[i][j], m)
      den += Math.pow(u[i][j], m)
    num / den

  membership = (x, c, m) ->
    u = []
    for i in [0..x.length - 1]
      u[i] = new Array(c.length)
      for j in [0..c.length - 1]
        accr = (prev, ck, k) ->
          jdist = Math.abs(x[i] - c[j])
          kdist = Math.abs(x[i] - c[k])
          prev + Math.pow(jdist / kdist, 2 / (m - 1))
        sum = c.reduce accr, 0
        u[i][j] = 1 / sum
    u

  maxDistance = (u1, u2) ->
    md = 0
    I = u1.length
    J = u1[0].length
    for i in [0..I - 1]
      for j in [0..J - 1]
        d = Math.abs(u1[i][j] - u2[i][j])
        if d > md
          md = d
    md

  fuzzyCMeans = (data, k, epsilon, m) ->

    if !k
      k = Math.ceil(Math.sqrt(data.length / 2))

    last = initializeU data.length, k
    next = null

    maxD = Infinity
    i = 0

    while maxD > epsilon
      i++
      c = new Array(k)
      for j in [0..k - 1]
        c[j] = centre data, last, j, m
      next = membership data, c, m
      maxD = maxDistance(next, last)
      last = next

    [c, next]

  cmeansToInput = (x, c, u) ->

    ordered = c.map (center, j) ->
      left = Number.NEGATIVE_INFINITY
      right = Number.POSITIVE_INFINITY
      for xi, i in x
        # Find the biggest x[i] such that x[i] < center
        # and u[i][j] is close to zero
        if xi < center && u[i][j] < 0.05 && xi > left
          left = xi
        # Find the smallest x[i] such that x[i] > center
        # and u[i][j] is close to zero
        if xi > center && u[i][j] < 0.05 && xi < right
          right = xi
      if left == Number.NEGATIVE_INFINITY
        [center, right]
      else if right == Number.POSITIVE_INFINITY
        [left, center]
      else
        [left, center, right]

    ordered.sort (a, b) ->
      if a[0] < b[0]
        -1
      else if a[0] > b[0]
        1
      else
        0

    named = {}

    for input, i in ordered
      name = SetNamer.nameSet i, c.length
      named[name] = input

    named

  cluster = (data, k, epsilon, m) ->
    [c, u] = fuzzyCMeans data, k, epsilon, m
    cmeansToInput data, c, u

module.exports = ClusterSetBuilder
