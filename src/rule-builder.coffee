# Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.io>
#
# All rights reserved.

class RuleBuilder

  @load: (name, dataset) ->
    cls = require "./rule-builder/#{name}"
    return new cls(dataset)

  constructor: (@dataset) ->

  buildRules: (dataset, sets) ->
    throw new Error("buildRules() unimplemented")

module.exports = RuleBuilder
