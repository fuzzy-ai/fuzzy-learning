# Copyright 2014 9165584 Canada Corporation <legal@fuzzy.io>
#
# All rights reserved.

eq = (a, b, epsilon) ->

  if !epsilon?
    epsilon = 1e-8

  absA = Math.abs a
  absB = Math.abs b
  diff = Math.abs(a - b)

  if (a == b)
    true
  else if (a == 0 || b == 0 || diff < Number.MIN_VALUE)
    # a or b is zero or both are extremely close to it
    # relative error is less meaningful here
    diff < epsilon
  else
    # use relative error
    diff / (absA + absB) < epsilon

leq = (a, b, epsilon) ->
  a < b or eq(a, b, epsilon)

geq = (a, b, epsilon) ->
  a > b or eq(a, b, epsilon)

module.exports = [eq, geq, leq]
