# Copyright 2014 9165584 Canada Corporation <legal@fuzzy.io>
#
# All rights reserved.

debug = require('debug')('learning:trainer')

class Trainer

  @load: (name) ->
    debug("loading #{name}")
    path = "./trainer/#{name}"
    debug("Path is #{path}")
    cls = require path
    debug("Require finished for #{name} (#{path})")
    return new cls()

  constructor: (@fc, @data = []) ->
    for row in @data
      @train row

  trainToDataset: (fc, dataset) ->
    debug("trainToDataset(...)")
    for i in [0..dataset.rows.length - 1]
      debug("training for row #{i}")
      row = dataset.row i
      @train fc, row
    fc

  train: (fc, row, e) ->
    null

  backtrack: ->
    null

module.exports = Trainer
