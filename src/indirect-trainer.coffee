# indirect-trainer.coffee
# Copyright 2017 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

debug = require('debug')('learning:indirect-trainer')

class IndirectTrainer

  @load: (name, props = {}) ->
    debug("loading #{name}")
    path = "./indirect-trainer/#{name}"
    debug("Path is #{path}")
    cls = require path
    debug("Require finished for #{name} (#{path})")
    return new cls(props)

  constructor: (@props) ->

  train: (fc, row, error) ->
    null

module.exports = IndirectTrainer
