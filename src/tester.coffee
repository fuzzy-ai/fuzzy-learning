# tester.coffee -- superclass for testing a controller with a dataset
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

class Tester

  @load: (name, props) ->
    Cls = require "./tester/#{name}"
    new Cls(props)

  constructor: (@props) ->

  test: (fc, dataset) ->

module.exports = Tester
