# set-namer.coffee
# Names sets based on the number of sets and name of the dimension
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

class SetNamer

  @nameSet: (i, count, dimensionName) ->

    setNames = SetNamer.nameSets count, dimensionName
    setNames[i]

  @nameSets: (count, dimensionName) ->

    # TODO: use dimension name for some sets, like
    # defaultSetNames(4, "temperature") -> ["cold", "cool", "warm", "hot"]

    switch count
      when 1
        ["medium"]
      when 2
        ["low", "high"]
      when 3
        ["low", "medium", "high"]
      when 4
        ["very low", "low", "high", "very high"]
      when 5
        ["very low", "low", "medium", "high", "very high"]
      when 6
        ["very low", "low", "medium low", "medium high", "high", "very high"]
      when 7
        ["very low", "low", "medium low", "medium",
        "medium high", "high", "very high"]
      when 8
        ["very very low", "very low", "low", "medium low",
        "medium high", "high", "very high", "very very high"]
      when 9
        ["very very low", "very low", "low", "medium low", "medium",
        "medium high", "high", "very high", "very very high"]
      else
        _.map [1..count - 1], (i) -> "set #{i}"

module.exports = SetNamer
