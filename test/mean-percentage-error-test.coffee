# mean-root-square-test.coffee -- Test the MeanSquaredErrorTester class
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert
debug = require("debug")("learning:mean-percentage-error-test")

testerBatch = require './tester-batch'

vows.describe "MeanPercentageErrorTester"
  .addBatch testerBatch "mean-percentage-error",
    "it looks like the right data structure": (err, results) ->
      assert.ifError err
      assert.isObject results
      assert.isNumber results.y
      debug(results)
  .export(module)
