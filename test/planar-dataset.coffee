# planar-dataset.coffee -- a set of planar data
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

ROW_COUNT = 100

colNames = ['x1', 'x2', 'y']
outputColNames = ['y']

rows = []

for i in [0..ROW_COUNT - 1]
  rows.push [3 * i, 4 * i, 7 * i]

module.exports =
  rows: rows
  colNames: colNames
  outputColNames: outputColNames
