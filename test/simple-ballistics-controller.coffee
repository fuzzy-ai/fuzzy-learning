# simple-ballistics-controller.coffee
# A simplistic ballistics controller with linear mapping
# Copyright 2016-2017 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

MAX_DISTANCE = 500
PI = Math.PI

module.exports =
  inputs:
    distance:
      veryLow: [0, MAX_DISTANCE / 4]
      low: [0, MAX_DISTANCE / 4, MAX_DISTANCE / 2]
      medium: [MAX_DISTANCE / 4, MAX_DISTANCE / 2, 3 * MAX_DISTANCE / 4]
      high: [MAX_DISTANCE / 2, 3 * MAX_DISTANCE / 4, MAX_DISTANCE]
      veryHigh: [3 * MAX_DISTANCE / 4, MAX_DISTANCE]
  outputs:
    angle:
      veryLow: [0, PI / 16]
      low: [0, PI / 16, PI / 8]
      medium: [PI / 16, PI / 8, 3 * PI / 16]
      high: [PI / 8, 3 * PI / 16, PI / 4]
      veryHigh: [3 * PI / 16, PI / 4]
  rules: [
    "distance INCREASES angle WITH 0.5"
  ]
