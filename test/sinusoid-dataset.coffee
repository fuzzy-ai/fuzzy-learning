# sinusoid-dataset.coffee -- a set of data for sinusoidal functions
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

_ = require 'lodash'

Dataset = require('../lib/dataset')

AMPLITUDE = 1
ZERO_AMPLITUDE = 0
WAVELENGTH = 1
PHASE = 0
CYCLES = 1

MAX_SAMPLES = 100

PI = Math.PI

sinusoidal = (A, D, lambda, phi, cycles, n) ->
  rows = new Array(n)
  interval = (lambda * cycles) / n
  for i in [0..n - 1]
    x = i * interval
    y = A * Math.sin(((2 * PI * x) / lambda) + phi) + D
    rows[i] = [x, y]
  new Dataset rows, ["x", "y"], ["y"]

module.exports =
  sinusoidal(AMPLITUDE, ZERO_AMPLITUDE, WAVELENGTH, PHASE, CYCLES, MAX_SAMPLES)
