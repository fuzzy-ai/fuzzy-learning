# custom-error-test.coffee -- Test the CustomErrorTester class
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert
debug = require("debug")("learning:custom-error-test")
FuzzyController = require '@fuzzy-ai/fuzzy-controller'

ErrorFunction = require('../lib/error-function')

MAX_DISTANCE = 500

class MissedByErrorFunction extends ErrorFunction
  constructor: (props) ->
    super(props)

  err: (inputs, actual) ->
    (MAX_DISTANCE * Math.sin(2 * actual.angle)) - inputs.distance

  slope: (inputs, actual) ->
    -1

vows.describe "ErrorFunctionTester"
  .addBatch
    'When we create a custom error tester':
      topic: ->
        Tester = require '../lib/tester'
        Tester.load 'error-function', {missedBy: new MissedByErrorFunction()}
      'it works': (err, tester) ->
        assert.ifError err
        assert.isObject tester
      'and we test a controller against a dataset':
        topic: (tester) ->
          fc = new FuzzyController require('./simple-ballistics-controller')
          dataset = require('./ballistics-dataset')
          tester.test fc, dataset
        'it works': (err, results) ->
          assert.ifError err
          assert.isObject results
          assert.isNumber results.missedBy
          debug(results)
  .export(module)
