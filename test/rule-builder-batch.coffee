# rule-builder-batch.coffee -- batch creator for testing RuleBuilder classes
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

vows = require 'perjury'
assert = vows.assert
debug = require('debug')('learning:rule-builder-batch')
# 100 rows of x1, x2, y

{rows, colNames, outputColNames} = require './planar-dataset'

js = JSON.stringify

ruleBuilderBatch = (name, setBuilderName = "cluster") ->
  batch = {}
  batch["When we load a #{name} builder"] =
    topic: ->
      RuleBuilder = require '../lib/rule-builder'
      RuleBuilder.load name
    'it works': (err, builder) ->
      assert.ifError err
      assert.isObject builder
    'it has the buildRules method': (err, builder) ->
      assert.ifError err
      assert.isObject builder
      assert.isFunction builder.buildRules
    'and we create a dataset':
      topic: (builder) ->
        Dataset = require '../lib/dataset'
        new Dataset rows, colNames, outputColNames
      'it works': (err, dataset) ->
        assert.ifError err
        assert.isObject dataset
      'and we create fuzzy sets':
        topic: (dataset, builder) ->
          SetBuilder = require '../lib/set-builder'
          setBuilder = SetBuilder.load setBuilderName
          setBuilder.buildSets dataset
        'it works': (err, sets) ->
          assert.ifError err
          assert.isObject sets, "returned sets are not an object: #{js sets}"
        'and we create fuzzy rules':
          topic: (sets, dataset, builder) ->
            debug(arguments)
            builder.buildRules dataset, sets
          'it works': (err, rules) ->
            assert.ifError err
            assert.isArray rules
          'and we create a new fuzzy controller':
            topic: (rules, sets) ->
              FuzzyController =
                require '@fuzzy-ai/fuzzy-controller'
              fc = new FuzzyController
                inputs: sets.inputs
                outputs: sets.outputs
                rules: rules
            'it works': (err, fc) ->
              assert.ifError err
              assert.isObject fc
  batch

module.exports = ruleBuilderBatch
