# controller-builder-batch.coffee -- batch creator for testing classes
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

vows = require 'perjury'
assert = vows.assert
debug = require('debug')('learning:controller-builder-batch')

js = JSON.stringify

controllerBuilderBatch = (type, datasetType = "planar", props = {}) ->
  batch = {}
  batch["When we load a #{type} ControllerBuilder with props"] =
    topic: ->
      ControllerBuilder = require '../lib/controller-builder'
      ControllerBuilder.load type, props
    'it works': (err, builder) ->
      assert.ifError err
      assert.isObject builder
    'it has the build method': (err, builder) ->
      assert.ifError err
      assert.isObject builder
      assert.isFunction builder.build
    "and we create a #{datasetType} dataset":
      topic: (builder) ->
        Dataset = require '../lib/dataset'
        {rows, colNames, outputColNames} = require "./#{datasetType}-dataset"
        new Dataset rows, colNames, outputColNames
      'it works': (err, dataset) ->
        assert.ifError err
        assert.isObject dataset
      'and we partition it':
        topic: (dataset) ->
          [train, test] = dataset.partition 2
          @callback null, train, test
          undefined
        'it works': (err, train, test) ->
          assert.ifError err
          assert.isObject train
          assert.isObject test
        'and we create a new fuzzy controller':
          topic: (train, test, dataset, builder) ->
            builder.build train
          'it works': (err, fc) ->
            assert.ifError err
            assert.isObject fc
          'and we test it':
            topic: (fc, train, test) ->
              Tester = require '../lib/tester'
              tester = Tester.load 'mean-percentage-error'
              tester.test fc, test
            'it works': (err, results) ->
              assert.ifError err
              debug results
              assert.isObject results
  batch

module.exports = controllerBuilderBatch
