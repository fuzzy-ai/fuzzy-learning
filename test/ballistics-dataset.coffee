# ballistics-dataset.coffee -- a set of data for ballistics distance-to-angle
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

_ = require 'lodash'

Dataset = require('../lib/dataset')

MAX_DISTANCE = 500
MAX_TESTING = 100

generateDataset = (n) ->
  rows = new Array(n)
  interval = MAX_DISTANCE / n
  for i in [0..n - 1]
    distance = Math.max(Math.min(interval * i, MAX_DISTANCE), 0)
    angle = 0.5 * Math.asin(distance / MAX_DISTANCE)
    rows[i] = [distance, angle]
  new Dataset rows, ["distance", "angle"], ["angle"]

module.exports = generateDataset MAX_TESTING
