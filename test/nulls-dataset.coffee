# nulls-dataset.coffee -- a dataset with null values
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

ROW_COUNT = 100

colNames = ['x1', 'x2', 'y']
outputColNames = ['y']

rows = []

for i in [0..ROW_COUNT - 1]
  if i % 5 == 0
    rows.push [3 * i, null, 3 * i]
  else if i % 5 == 1
    rows.push [null, 4 * i, 4 * i]
  else
    rows.push [3 * i, 4 * i, 7 * i]

module.exports =
  rows: rows
  colNames: colNames
  outputColNames: outputColNames
