# lfe-test.coffee
# Test LFE rules builder
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>

vows = require 'perjury'
assert = vows.assert
debug = require('debug')('learning:flfe-test')

controllerBuilderBatch = require './controller-builder-batch'

props = {ruleBuilderType: 'flfe', setBuilderType: 'cluster'}

vows.describe 'FLFERuleBuilder'
  .addBatch controllerBuilderBatch('compound', 'planar', props)
  .addBatch controllerBuilderBatch('compound', 'ballistics', props)
  .addBatch controllerBuilderBatch('compound', 'kim-function', props)
  .export module
