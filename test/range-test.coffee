# range-test.coffee
# Test range learning
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>

vows = require 'perjury'
assert = vows.assert
debug = require('debug')('learning:range-test')

setBuilderBatch = require './set-builder-batch'

vows.describe 'RangeSetBuilder'
  .addBatch setBuilderBatch('range')
  .export module
