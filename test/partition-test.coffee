# partition-test.coffee
# Test partition set builder
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>

vows = require 'perjury'
assert = vows.assert
debug = require('debug')('learning:range-test')

setBuilderBatch = require './set-builder-batch'

vows.describe 'PartitionSetBuilder'
  .addBatch setBuilderBatch('partition')
  .export module
