# index-test.coffee
# test that the index works
# Copyright 2017 Fuzzy.ai
# All rights reserved

vows = require 'perjury'
assert = vows.assert

attrTest = (attrName) ->
  (err, index) ->
    assert.ifError err
    assert.isObject index
    assert.isFunction index[attrName]

vows.describe 'index'
  .addBatch
    'When we require the index':
      topic: ->
        try
          index = require '../lib/index'
          @callback null, index
        catch error
          @callback error
        undefined
      'it works': (err, index) ->
        assert.ifError err
        assert.isObject index
      'it has a ControllerBuilder attribute': attrTest 'ControllerBuilder'
      'it has a Dataset attribute': attrTest 'Dataset'
      'it has a RuleBuilder attribute': attrTest 'RuleBuilder'
      'it has a SetBuilder attribute': attrTest 'SetBuilder'
      'it has a Tester attribute': attrTest 'Tester'
      'it has a Trainer attribute': attrTest 'Trainer'
  .export module
