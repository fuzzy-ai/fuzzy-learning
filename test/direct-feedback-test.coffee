# direct-feedback-test.coffee -- test with direct feedback
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>

fs = require 'fs'

vows = require 'perjury'
assert = vows.assert
_ = require 'lodash'
debug = require('debug')('learning:direct-feedback-test')

FuzzyController = require '@fuzzy-ai/fuzzy-controller'
[testing, training] = require('./ballistics-dataset').partition(2)

MAX_TESTING = 100
MAX_TRAINING = 20

js = JSON.stringify
def = require './simple-ballistics-controller'

vows.describe "Direct feedback training for hand-built agent"
  .addBatch
    'When we create a ballistics fuzzy controller':
      topic: ->
        new FuzzyController _.cloneDeep(def)
      'it works': (err, fc) ->
        assert.ifError err
        assert.isObject fc
        assert.instanceOf fc, FuzzyController
      'and we test the controller':
        topic: (fc) ->
          Tester = require '../lib/tester'
          tester = Tester.load 'mean-percentage-error'
          tester.test fc, testing
        'it works': (err, mpe) ->
          assert.ifError err
          assert.isObject mpe
          assert.isNumber mpe.angle
          debug "MPE = #{mpe.angle}"
        'and we train the controller':
          topic: (mpe, fc) ->
            Trainer = require '../lib/trainer'
            gd = Trainer.load 'gradient-descent'
            gd.trainToDataset fc, training
          'it works': (err, newFC) ->
            assert.ifError err
            assert.isObject newFC
            assert.instanceOf newFC, FuzzyController
          'and we compare the old and new controllers':
            topic: (newFC, oldMPE, oldFC) ->
              oldJSON = def
              newJSON = _.omit(newFC.toJSON(), 'parsed_rules')
              oldArray = (new FuzzyController(def)).toArray()
              newArray = newFC.toArray()
              debug "oldFC.toArray() = #{js oldArray}"
              debug "newFC.toArray() = #{js newArray}"
              delta = _.map(newArray, (val, i) ->
                (val - oldArray[i]) / oldArray[i])
              debug "delta = #{js delta}"
              [oldJSON, newJSON]
            'it is changed': (err, pair) ->
              assert.ifError err
              assert.isArray pair
              [oldJSON, newJSON] = pair
              debug(JSON.stringify(oldJSON))
              debug(JSON.stringify(newJSON))
              assert.notDeepEqual newJSON, oldJSON,
                "new controller unchanged from old controller"
          'and we test the trained controller':
            topic: (newFC, oldMPE, oldFC) ->
              Tester = require '../lib/tester'
              tester = Tester.load 'mean-percentage-error'
              newMPE = tester.test newFC, testing
              [oldMPE, newMPE]
            'it works': (err, pair) ->
              assert.ifError err
              assert.isArray pair
              [oldMPE, newMPE] = pair
              assert.isObject oldMPE
              assert.isNumber oldMPE.angle
              assert.isObject newMPE
              assert.isNumber newMPE.angle
              debug "oldMPE = #{js oldMPE}"
              debug "newMPE = #{js newMPE}"
              assert.lesser newMPE.angle, (oldMPE.angle * 2),
                "Trained controller not better " +
                " (#{newMPE.angle} vs #{oldMPE.angle})"
  .export module
