# trainer-batch.coffee -- batch for Trainer classes
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

vows = require 'perjury'
assert = vows.assert
_ = require "lodash"
debug = require('debug')('learning:trainer-batch')

FuzzyController = require '@fuzzy-ai/fuzzy-controller'

defaultOptions =
  setBuilderType: "cluster"
  ruleBuilderType: "lfe"
  datasetType: "kim-function"

js = JSON.stringify

trainerBatch = (name, options = {}) ->
  options = _.defaults options, defaultOptions
  {setBuilderType, ruleBuilderType, datasetType} = options
  batch = {}
  batch["When we create a #{name} trainer"] =
    topic: ->
      Trainer = require '../lib/trainer'
      trainer = Trainer.load name
      trainer
    'it works': (err, trainer) ->
      assert.ifError err
      assert.isObject trainer
    'and we create datasets':
      topic: (trainer) ->
        Dataset = require "../lib/dataset"
        dataset = require "./#{datasetType}-dataset"
        if !(dataset instanceof Dataset)
          {rows, colNames, outputColNames} = dataset
          dataset = new Dataset rows, colNames, outputColNames
        dataset.partition 3
      'it works': (err, subsets) ->
        assert.ifError err
        assert.isArray subsets
      'and we create a controller':
        topic: (subsets, trainer) ->
          dataset = subsets[0]
          ControllerBuilder = require "../lib/controller-builder"
          props =
            setBuilderType: setBuilderType
            ruleBuilderType: ruleBuilderType
          cb = ControllerBuilder.load 'compound', props
          fc = cb.build dataset
          fc
        "it works": (err, fc) ->
          assert.ifError err
          assert.isObject fc
          debug(JSON.stringify(fc.toJSON(), "\n"))
        "and we test the controller":
          topic: (fc, subsets) ->
            tester = require("../lib/tester").load "mean-percentage-error"
            tester.test fc, subsets[1]
          "it works": (err, results) ->
            assert.ifError err
            assert.isObject results
            for name, value of results
              assert.isNumber value, "Value for #{name} not a number: #{value}"
          "and we optimize the controller":
            topic: (testResults, fc, subsets, trainer) ->
              dataset = subsets[2]
              dup = new FuzzyController(fc.toJSON())
              trainer.trainToDataset dup, dataset
            "it works": (err, fc) ->
              debug 'Checking that optimization works'
              debug 'Checking that results are an error'
              debug err
              assert.ifError err, "Optimizing generated an error"
              debug 'Checking that there are some results'
              assert.isObject fc, "Results of training is not an object"
              debug 'Checking that the results are a FuzzyController'
              assert.instanceOf fc, FuzzyController, "Not a FuzzyController"
              debug(JSON.stringify(fc.toJSON(), "\n"))
            "and we compare the controllers":
              topic: (trained, testResults, original) ->
                [original.toJSON(), trained.toJSON()]
              "they are different": (err, pair) ->
                assert.ifError err
                assert.isArray pair
                [oldJSON, newJSON] = pair
                assert.isObject oldJSON
                assert.isObject newJSON
                assert.notDeepEqual oldJSON, newJSON
            "and we test the controller again":
              topic: (trained, testResults, original, subsets) ->
                tester = require("../lib/tester").load "mean-percentage-error"
                tester.test trained, subsets[1]
              "it works": (err, results) ->
                assert.ifError err
                assert.isObject results
                for name, value of results
                  assert.isNumber value,
                    "Test results for #{name} not a number: #{value}"
              "and we compare the test results":
                topic: (after, trained, before) ->
                  debug(js after)
                  debug(js before)
                  [before, after]
                "it works": (err, both) ->
                  assert.ifError err
                  # Doesn't have to be perfect but don't go over 2x the original
                  [before, after] = both
                  assert.isObject before
                  assert.isObject after
                  debug(js before)
                  debug(js after)
                  for name, value of before
                    assert.lesser after[name], value * 2
  batch

module.exports = trainerBatch
