# error-function-batch.coffee -- test the ErrorFunction class
# Copyright 2017 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

vows = require 'perjury'
assert = vows.assert

debug = require('debug')('learning:error-function-batch')

errorFunctionBatch = (type, props) ->
  batch =
    "When we load an #{type} error function":
      topic: ->
        debug(type)
        debug(props)
        ErrorFunction = require '../lib/error-function'
        ErrorFunction.load type, props
      'it works': (err, efunc) ->
        assert.ifError err
        assert.isObject efunc
      'it has an err() method': (err, efunc) ->
        assert.ifError err
        assert.isObject efunc
        assert.isFunction efunc.err
      'it has a slope() method': (err, efunc) ->
        assert.ifError err
        assert.isObject efunc
        assert.isFunction efunc.slope
  batch

module.exports = errorFunctionBatch
