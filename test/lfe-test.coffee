# lfe-test.coffee
# Test LFE rules builder
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>

vows = require 'perjury'
assert = vows.assert
debug = require('debug')('learning:lfe-test')

controllerBuilderBatch = require './controller-builder-batch'

props = {ruleBuilderType: 'lfe', setBuilderType: 'cluster'}

vows.describe 'LFERuleBuilder'
  .addBatch controllerBuilderBatch('compound', 'planar', props)
  .addBatch controllerBuilderBatch('compound', 'ballistics', props)
  .addBatch controllerBuilderBatch('compound', 'kim-function', props)
  .addBatch controllerBuilderBatch('compound', 'nulls', props)
  .export module
