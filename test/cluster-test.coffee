# cluster-test.coffee
# Test Cluster learning
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>

vows = require 'perjury'
assert = vows.assert
debug = require('debug')('learning:cluster-test')

setBuilderBatch = require './set-builder-batch'

vows.describe 'ClusterSetBuilder'
  .addBatch setBuilderBatch('cluster')
  .export module
