# gradient-descent-test.coffee -- test the GradientDescentTrainer class
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

vows = require 'perjury'
assert = vows.assert

trainerBatch = require './trainer-batch'

vows.describe('GradientDescentTrainer for nulls dataset')
  .addBatch(trainerBatch('gradient-descent', {datasetType: "nulls"}))
  .export(module)
