# dataset-test.coffee -- test Dataset class
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

vows = require 'perjury'
assert = vows.assert
debug = require('debug')('learning:dataset-test')
_ = require 'lodash'

moduleBatch = (name) ->
  {rows, colNames, outputColNames} = require "./#{name}-dataset"

  batch =
    'When we load the Dataset class':
      topic: ->
        require '../lib/dataset'
      'it works': (err, Dataset) ->
        assert.ifError err
        assert.isFunction Dataset
      'and we instantiate a Dataset':
        topic: (Dataset) ->
          new Dataset rows, colNames, outputColNames
        'it works': (err, dataset) ->
          assert.ifError err
          assert.isObject dataset
        'and we examine the dataset':
          topic: (dataset) ->
            dataset
          'it has a rows member': (err, dataset) ->
            assert.ifError err
            assert.isArray dataset.rows
            assert.lengthOf dataset.rows, rows.length
            for i in [0..rows.length - 1]
              for j in [0..rows[i].length - 1]
                a = dataset.rows[i][j]
                b = rows[i][j]
                assert.equal a, b,
                  "dataset.rows[#{i}][#{j}] = #{a}, rows[i][j] = #{b}"
          'it has a colNames member': (err, dataset) ->
            assert.ifError err
            assert.isArray dataset.colNames
          'it has an outputColNames member': (err, dataset) ->
            assert.ifError err
            assert.isArray dataset.outputColNames
          'it has a row method': (err, dataset) ->
            assert.ifError err
            assert.isFunction dataset.row
          'it has a getColumn method': (err, dataset) ->
            assert.ifError err
            assert.isFunction dataset.getColumn
          'it has an isOutputColumn method': (err, dataset) ->
            assert.ifError err
            assert.isFunction dataset.isOutputColumn
          'and we call the row method':
            topic: (dataset) ->
              dataset.row 50
            'it works': (err, row) ->
              assert.ifError err
              assert.isObject row, 'row is not an object'
              assert.isObject row.data, 'row.data is not an object'
              assert.isObject row.input, 'row.inputs is not an object'
              assert.isObject row.output, 'row.outputs is not an object'
          'and we call the getColumn method':
            topic: (dataset) ->
              dataset.getColumn 'x2'
            'it works': (err, column) ->
              assert.ifError err
              assert.isArray column
              assert.lengthOf column, 100
              for n, i in column
                msg = "Item at index #{i} (#{n}) is not a number or null"
                assert _.isNumber(n) or _.isNull(n), msg
                x2 = rows[i][1]
                assert.equal n, x2,
                  "Item at index #{i} (#{n}) does not match rows data #{x2}"
          'and we call the isOutputColumn method on an input column':
            topic: (dataset) ->
              dataset.isOutputColumn 'x2'
            'it works': (err, result) ->
              assert.ifError err
              assert.isFalse result
          'and we call the isOutputColumn method on an output column':
            topic: (dataset) ->
              dataset.isOutputColumn 'y'
            'it works': (err, result) ->
              assert.ifError err
              debug result
              assert.isTrue result
  batch

vows.describe 'Dataset'
  .addBatch moduleBatch("planar")
  .addBatch moduleBatch("nulls")
  .addBatch
    'When we load the Dataset class':
      topic: ->
        require '../lib/dataset'
      'it works': (err, Dataset) ->
        assert.ifError err
        assert.isFunction Dataset
      'it has a fromCSV method': (err, Dataset) ->
        assert.ifError err
        assert.isFunction Dataset
        assert.isFunction Dataset.fromCSV
      'and we use the fromCSV class method to create a dataset':
        topic: (Dataset) ->
          # z = 2x + 3y
          csv = """
          x,y,z
          5,9,37
          3,2,12
          0,0,0
          4,4,20
          """
          Dataset.fromCSV csv
        "it works": (err, dataset) ->
          assert.ifError err
          assert.isObject dataset
        "it has the right column names": (err, dataset) ->
          assert.ifError err
          assert.isObject dataset
          assert.isArray dataset.colNames
          assert.deepEqual dataset.colNames, ["x", "y", "z"]
          assert.isArray dataset.outputColNames
          assert.deepEqual dataset.outputColNames, ["z"]
        "it has the right rows": (err, dataset) ->
          assert.ifError err
          assert.isObject dataset
          assert.isArray dataset.rows
          assert.lengthOf dataset.rows, 4
          for row in dataset.rows
            assert.isArray row
            assert.lengthOf row, 3
            for item in row
              assert.isNumber item
  .addBatch
    'When we load the Dataset class':
      topic: ->
        require '../lib/dataset'
      'it works': (err, Dataset) ->
        assert.ifError err
        assert.isFunction Dataset
      'and we instantiate a dataset':
        topic: (Dataset) ->
          rows = []
          for i in [0..99]
            rows.push [i, i * i]
          new Dataset rows, ["x", "y"], ["y"]
        "it works": (err, dataset) ->
          assert.ifError err
          assert.isObject dataset
        "it has a partition() method": (err, dataset) ->
          assert.ifError err
          assert.isObject dataset
          assert.isFunction dataset.partition
        "and we ask it to divide into 3 subsets":
          topic: (dataset) ->
            dataset.partition 3
          "it works": (err, subsets) ->
            assert.ifError err
            assert.isArray subsets
            assert.lengthOf subsets, 3
            sum = 0
            seen = []
            for subset in subsets
              assert.isObject subset
              assert.isArray subset.rows
              sum += subset.rows.length
              assert _.inRange(subset.length, 33, 34)
              assert.deepEqual subset.colNames, ["x", "y"]
              assert.deepEqual subset.outputColNames, ["y"]
              for row in subset.rows
                assert.equal seen.indexOf(row[0]), -1
                seen.push row[0]
            assert.equal sum, 100
          "and we ask to divide into 3 subsets again":
            topic: (subsets, dataset) ->
              @callback null, subsets, dataset.partition(3)
              undefined
            "it is idempotent": (err, originals, duplicates) ->
              assert.ifError err
              assert.isArray originals
              assert.lengthOf originals, 3
              assert.isArray duplicates
              assert.lengthOf duplicates, 3
              for original, i in originals
                assert.isObject original
                assert.isArray original.rows
                duplicate = duplicates[i]
                assert.isObject duplicate
                assert.isArray duplicate.rows
                for row, j in original.rows
                  assert.deepEqual original.rows[j], duplicate.rows[j]
  .export module
