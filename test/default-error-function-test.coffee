# default-error-function-test.coffee
# Test the default error function
# Copyright 2017 Fuzzy.ai <legal@fuzzy.ai>

vows = require 'perjury'
assert = vows.assert

errorFunctionBatch = require './error-function-batch'

vows.describe 'DefaultErrorFunction'
  .addBatch errorFunctionBatch('default', {expected: {x: 7}})
  .addBatch
    'When we create a DefaultErrorFunction':
      topic: ->
        ErrorFunction = require('../lib/error-function')
        ErrorFunction.load 'default', {expected: {x: 3}}
      'it works': (err, efunc) ->
        assert.ifError err
        assert.isObject efunc
      'and we calculate the error value':
        topic: (efunc) ->
          efunc.err {y: 1}, {x: 4}
        'it works': (err, results) ->
          assert.ifError err
          assert.isObject results
          assert.isNumber results.x
          assert.inDelta results.x, 0.5, 0.01
      'and we calculate the slope':
        topic: (efunc) ->
          efunc.slope {y: 1}, {x: 4}
        'it works': (err, results) ->
          assert.ifError err
          assert.isObject results
          assert.isNumber results.x
          assert.inDelta results.x, 1, 0.01
  .export module
