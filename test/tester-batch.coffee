# tester-batch.coffee -- batch for a Tester implementation
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

_ = require "lodash"
vows = require 'perjury'
assert = vows.assert

testerBatch = (name, rest) ->
  batch = {}
  batch["When we create a #{name} tester"] =
    topic: ->
      Tester = require "../lib/tester"
      Tester.load name
    "it works": (err, tester) ->
      assert.ifError err
      assert.isObject tester
      assert.instanceOf tester, require("../lib/tester")
    "it has a test() method": (err, tester) ->
      assert.ifError err
      assert.isObject tester
      assert.isFunction tester.test
    "and we test a controller with a dataset":
      topic: (tester) ->
        FuzzyController = require '@fuzzy-ai/fuzzy-controller'
        fc = new FuzzyController
          inputs:
            x:
              low: [0, 50]
              medium: [0, 50, 100]
              high: [50, 100]
          outputs:
            y:
              low: [0, 100]
              medium: [0, 100, 200]
              high: [100, 200]
          rules: ["x INCREASES y"]
        rows = []
        for i in [0..100]
          rows.push [i, 2 * i]
        Dataset = require "../lib/dataset"
        dataset = new Dataset rows, ["x", "y"], ["y"]
        tester.test fc, dataset
      "it works": (err, results) ->
        assert.ifError err
        assert.isDefined results
        assert.isNotNull results

  if rest?
    a = "When we create a #{name} tester"
    b = "and we test a controller with a dataset"
    _.assign batch[a][b], rest

  batch

module.exports = testerBatch
