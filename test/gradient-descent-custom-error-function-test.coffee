# gradient-descent-custom-error-function-test.coffee
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

vows = require 'perjury'
assert = vows.assert
debug = require('debug')('learning:gradient-descent-custom-error-function-test')
FuzzyController = require '@fuzzy-ai/fuzzy-controller'
def = require './simple-ballistics-controller'
ErrorFunction = require '../lib/error-function'

js = JSON.stringify
MAX_DISTANCE = 500

class MissedByErrorFunction extends ErrorFunction
  constructor: (props) ->
    super(props)
    @fc = @props.fc

  err: (inputs, actual) ->
    (MAX_DISTANCE * Math.sin(2 * actual.angle)) - inputs.distance

  slope: (inputs, actual) ->
    inpslo = @fc.inputSlope('distance', 'angle', inputs)
    ((MAX_DISTANCE * 2 * Math.cos(2 * actual.angle)) * inpslo) - 1

vows.describe('GradientDescentTrainer with custom error function')
  .addBatch
    'When we create a GradientDescentTrainer':
      topic: ->
        Trainer = require '../lib/trainer'
        trainer = Trainer.load 'gradient-descent'
        trainer
      'it works': (err, trainer) ->
        assert.ifError err
        assert.isObject trainer
      'and we create datasets':
        topic: (trainer) ->
          dataset = require "./ballistics-dataset"
          dataset.partition 2
        'it works': (err, subsets) ->
          assert.ifError err
          assert.isArray subsets
        'and we create a controller':
          topic: (subsets, trainer) ->
            new FuzzyController def
          "it works": (err, fc) ->
            assert.ifError err
            assert.isObject fc
          "and we test the controller":
            topic: (fc, subsets) ->
              Tester = require("../lib/tester")
              ef = new MissedByErrorFunction(fc)
              tester = Tester.load "error-function", {missedBy: ef}
              tester.test fc, subsets[0]
            "it works": (err, results) ->
              assert.ifError err
              assert.isObject results
              for name, value of results
                msg = "Value for #{name} not a number: #{value}"
                assert.isNumber value, msg
            "and we optimize the controller":
              topic: (testResults, fc, subsets, trainer) ->
                dataset = subsets[1]
                dup = new FuzzyController(fc.toJSON())
                ef = new MissedByErrorFunction(fc)
                trainer.trainToDataset dup, dataset, ef
              "it works": (err, fc) ->
                debug 'Checking that optimization works'
                debug 'Checking that results are an error'
                debug err
                assert.ifError err, "Optimizing generated an error"
                debug 'Checking that there are some results'
                assert.isObject fc, "Results of training is not an object"
                debug 'Checking that the results are a FuzzyController'
                assert.instanceOf fc, FuzzyController, "Not a FuzzyController"
                debug(JSON.stringify(fc.toJSON(), "\n"))
              "and we compare the controllers":
                topic: (trained, testResults, original) ->
                  [original.toJSON(), trained.toJSON()]
                "they are different": (err, pair) ->
                  assert.ifError err
                  assert.isArray pair
                  [oldJSON, newJSON] = pair
                  assert.isObject oldJSON
                  assert.isObject newJSON
                  assert.notDeepEqual oldJSON, newJSON
              "and we test the controller again":
                topic: (trained, testResults, original, subsets) ->
                  Tester = require("../lib/tester")
                  ef = new MissedByErrorFunction(trained)
                  tester = Tester.load "error-function", {missedBy: ef}
                  tester.test trained, subsets[0]
                "it works": (err, results) ->
                  assert.ifError err
                  assert.isObject results
                  for name, value of results
                    msg = "Test results for #{name} not a number: #{value}"
                    assert.isNumber value, msg
                "and we compare the test results":
                  topic: (after, trained, before) ->
                    debug(js after)
                    debug(js before)
                    [before, after]
                  "it works": (err, both) ->
                    assert.ifError err
                    # Doesn't have to be perfect but don't go over 2x the
                    # original
                    [before, after] = both
                    assert.isObject before
                    assert.isObject after
                    debug(js before)
                    debug(js after)
                    for name, value of before
                      assert.lesser after[name], (value * 2)
  .export(module)
