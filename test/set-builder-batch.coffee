# set-builder-batch.coffee -- batch creator for testing SetBuilder classes
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved

vows = require 'perjury'
assert = vows.assert

# 100 rows of x1, x2, y

{rows, colNames, outputColNames} = require './planar-dataset'

js = JSON.stringify

assertValidSets = (sets, name) ->
  for setName, shape in sets
    assert.isString setName, "set name not a string #{js setName}"
    assert.isArray shape, "#{name}/#{setName} shape is not an array #{js shape}"
    assert shape.length >= 2 and shape.length <= 4,
      "#{name}/#{setName} shape size not between 2 and 4: #{js shape}"
    for value, i in shape
      assert.isNumber value,
        "#{name}/#{setName} value #{i} is not a number: #{js value}"
      if i > 0
        assert.greater value, shape[i - 1],
          "#{name}/#{setName} value #{i} #{value} is not greater than " +
          "previous value #{shape[i - 1]}"


setBuilderBatch = (name) ->
  batch = {}
  batch["When we load a #{name} builder"] =
    topic: ->
      try
        SetBuilder = require '../lib/set-builder'
        builder = SetBuilder.load name
        @callback null, builder
      catch err
        @callback err
      undefined
    'it works': (err, builder) ->
      assert.ifError err
      assert.isObject builder
    'it has the buildSets method': (err, builder) ->
      assert.ifError err
      assert.isObject builder
      assert.isFunction builder.buildSets
    'it has the buildSetsForColumn method': (err, builder) ->
      assert.ifError err
      assert.isObject builder
      assert.isFunction builder.buildSetsForColumn
    'and we build fuzzy sets':
      topic: (builder) ->
        try
          Dataset = require '../lib/dataset'
          dataset = new Dataset rows, colNames, outputColNames
          sets = builder.buildSets dataset
          @callback null, sets
        catch err
          @callback err
        undefined
      'it works': (err, sets) ->
        assert.ifError err
        assert.isObject sets, "returned sets are not an object: #{js sets}"
        assert.isObject sets.inputs,
          "sets.inputs is not an object: #{js sets.inputs}"
        assert.isObject sets.inputs.x1,
          "sets.inputs.x1 is not an object: #{js sets.inputs.x1}"
        assertValidSets sets.inputs.x1, "x1"
        assert.isObject sets.inputs.x2,
          "sets.inputs.x2 is not an object: #{js sets.inputs.x2}"
        assertValidSets sets.inputs.x2, "x2"
        assert.isUndefined sets.inputs.y,
          "sets.inputs.y is not undefined: #{js sets.inputs.y}"
        assert.isObject sets.outputs,
          "sets.outputs is not an object: #{js sets.outputs}"
        assert.isObject sets.outputs.y,
          "sets.outputs.y is not an object: #{js sets.outputs.y}"
        assertValidSets sets.outputs.y, "y"

  batch

module.exports = setBuilderBatch
