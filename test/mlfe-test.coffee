# mlfe-test.coffee
# Test MLFE rules builder
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>

vows = require 'perjury'
assert = vows.assert
debug = require('debug')('learning:mlfe-test')

controllerBuilderBatch = require './controller-builder-batch'

vows.describe 'MLFEControllerBuilder'
  .addBatch controllerBuilderBatch('mlfe', 'planar')
  .addBatch controllerBuilderBatch('mlfe', 'ballistics')
  .addBatch controllerBuilderBatch('mlfe', 'kim-function')
  .export module
