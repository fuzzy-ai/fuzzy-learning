# fuzzy-learning

This is a collection of useful classes for creating new fuzzy controllers from
data and for optimizing existing fuzzy controllers based on results.

You can get to the classes by requiring the module. Each important class is a
property of the index.

```
fl = require 'fuzzy-learning'
Dataset = fl.Dataset
```

## Dataset

This is an abstraction for a number of rows, each of which maps 1 or more inputs
to 1 or more outputs.

## ControllerBuilder

This is an abstract superclass for creating new FuzzyController instances from
a dataset.

You can create a new controller using the `load` method:

```
  builder = ControllerBuilder.load 'mlfe', {'tolerance': 0.1}
  fc = builder.build dataset
```

There are three main controller builder classes:

- ***MLFE*** does the "modified LFE" algorithm from Passino Yurkovich.
- ***Wu-Chen*** does the algorithm from Wu Chen 1999.
- ***Compound*** uses separate RuleBuilder and SetBuilder classes to first
  build input and output sets, then developer rules about them.

## SetBuilder

Abstract superclass for building input and output sets from a Dataset.

## RuleBuilder

Abstract superclass for building rules from a Dataset.

## Tester

Superclass for testing a controller against a dataset.

## Trainer

Superclass for training a controller against a dataset.
