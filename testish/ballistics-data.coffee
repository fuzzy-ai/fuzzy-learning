# ballistics-data.coffee
# Dataset with distance,correct angle,linear model angle

FuzzyController = require '@fuzzy-ai/fuzzy-controller'

PI = Math.PI

if process.argv.length > 2
  maxDistance = parseInt process.argv[2], 10
else
  maxDistance = 500

fc = new FuzzyController
  inputs:
    distance:
      veryLow: [0, maxDistance/4]
      low: [0, maxDistance/4, maxDistance/2]
      medium: [maxDistance/4, maxDistance/2, 3*maxDistance/4]
      high: [maxDistance/2, 3*maxDistance/4, maxDistance]
      veryHigh: [3*maxDistance/4, maxDistance]
  outputs:
    angle:
      veryLow: [0, PI/16]
      low: [0, PI/16, PI/8]
      medium: [PI/16, PI/8, 3*PI/16]
      high: [PI/8, 3*PI/16, PI/4]
      veryHigh: [3*PI/16, PI/4]
  rules: [
    "distance INCREASES angle WITH 0.5"
  ]

console.log "Distance,Actual,Expected"

for d in [0..maxDistance] by maxDistance/100 
  {angle} = fc.evaluate {distance: d}
  expected = 0.5 * Math.asin(d/maxDistance)
  console.log "#{d},#{angle},#{expected}"
