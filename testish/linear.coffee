# linear.coffee
# Test using learning a linear relation

util = require 'util'

_ = require 'lodash'

argv = require('yargs')
  .usage('Usage: $0 -m<slope> -b<intercept>')
  .help('h')
  .alias('h', 'help')
  .default('m', 1)
  .alias('m', 'slope')
  .number('m')
  .describe('m', 'slope of the line')
  .default('b', 0)
  .alias('b', 'intercept')
  .number('b')
  .describe('b', 'y-axis intercept of the line')
  .default('s', 0)
  .alias('s', 'samples')
  .number('s')
  .describe('s', 'Number of samples to retrain')
  .default('i', 1)
  .alias('i', 'iterations')
  .number('i')
  .describe('i', 'Number of iterations to train')
  .argv

trainToFunction = require './train-to-function'

ITERATIONS = 1
SAMPLES = 0

BOUNDARY = 100

initial =
  inputs:
    x:
      veryLow: [0, BOUNDARY/4]
      low: [0, BOUNDARY/4, BOUNDARY/2]
      medium: [BOUNDARY/4, BOUNDARY/2, 3*BOUNDARY/4]
      high: [BOUNDARY/2, 3*BOUNDARY/4, BOUNDARY]
      veryHigh: [3*BOUNDARY/4, BOUNDARY]
  outputs:
    y:
      veryLow: [0, BOUNDARY/4]
      low: [0, BOUNDARY/4, BOUNDARY/2]
      medium: [BOUNDARY/4, BOUNDARY/2, 3*BOUNDARY/4]
      high: [BOUNDARY/2, 3*BOUNDARY/4, BOUNDARY]
      veryHigh: [3*BOUNDARY/4, BOUNDARY]
  rules: [
    "if x is veryLow then y is veryLow WITH 0.5"
    "if x is low then y is low WITH 0.5"
    "if x is medium then y is medium WITH 0.5"
    "if x is high then y is high WITH 0.5"
    "if x is veryHigh then y is veryHigh WITH 0.5"
  ]

main = (argv) ->

  {m, b, i, s} = argv

  func = (x) -> m * x + b

  json = trainToFunction initial, func, 0, BOUNDARY, i, s

  console.log util.inspect(_.omit(json, 'parsed_rules'), {depth: null})

main(argv)
