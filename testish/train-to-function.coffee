# train-to-function.coffee
# Test using learning a linear relation

fs = require 'fs'
assert = require 'assert'

_ = require 'lodash'

FuzzyController = require '@fuzzy-ai/fuzzy-controller'
GradientDescentTrainer = require '../lib/gradient-descent'

# Make this deterministic for testing

values = [
  39,
  43,
  1,
  2,
  94,
  22,
  98,
  59,
  41,
  71,
  67,
  45,
  81,
  68,
  11,
  65,
  9,
  96,
  69,
  77,
  55,
  26,
  79,
  32,
  6,
  49,
  92,
  34,
  0,
  20,
  97,
  66,
  85,
  78,
  82,
  56,
  95,
  87,
  63,
  50,
  48,
  4,
  57,
  8,
  28,
  5,
  21,
  12,
  37,
  64,
  46,
  42,
  73,
  61,
  62,
  40,
  10,
  17,
  80,
  88,
  44,
  18,
  24,
  14,
  29,
  27,
  53,
  38,
  93,
  19,
  83,
  52,
  72,
  58,
  31,
  84,
  74,
  23,
  60,
  76,
  99,
  3,
  100,
  91,
  15,
  36,
  70,
  16,
  51,
  54,
  75,
  35,
  7,
  89,
  13,
  33,
  25,
  90,
  47,
  86,
  30 ]

snapshot = (fc, i, func, low, high) ->
  data = ""
  for x in [low..high] by 1
    ay = fc.evaluate({x: x}).y
    y = func x
    data += "#{x}\t#{ay}\t#{y}\t#{Math.abs(y - ay)/Math.abs(y)}\n"
  fs.writeFileSync "/tmp/#{i}.dat", data, "utf8"

performance = (fc, i, func, low, high) ->
  sum = 0
  for x in [low..high]
    ay = fc.evaluate({x: x}).y
    y = func x
    err = 0.5 * Math.pow(ay - y, 2)
    sum += err
  sum

trainToFunction = (initial, func, low=0, high=100, iterations=1, samples=3) ->

  fc = new FuzzyController initial
  tr = new GradientDescentTrainer fc, [], iterations, samples

  i = 0
  best = fc.toArray()

  p = performance fc, i, func, low, high
  bestp = p

  snapshot fc, i, func, low, high

  if low == 0 && high == 100
    tset = values
  else
    tset = _.shuffle([low..high])

  for method in ['trainOutputParameters', 'trainInputParameters', 'trainRuleParameters']
    for x in tset
      # The correct angle
      y = func x
      tr[method]({inputs: {x: x}, outputs: {y: y}})
      i++
      p = performance fc, i, func, low, high
      if p < bestp
        best = fc.toArray()
        bestp = p
      else if p > bestp * 2
        # reset to best
        fc.fromArray best
        p = bestp
      console.log p
      snapshot fc, i, func, low, high
      # console.log require('util').inspect(_.omit(fc.toJSON(), "parsed_rules"), {depth: null})
    fc.fromArray best

  fc.toJSON()

module.exports = trainToFunction
