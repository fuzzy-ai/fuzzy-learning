# ballistics.coffee
# Test using ballistics

assert = require 'assert'

_ = require 'lodash'

trainToFunction = require './train-to-function'

# This is a fuzzy artillery officer, in charge of a cannon. The cannon has
# a fixed muzzle velocity of 500 m/s and the projectile has a mass of 5 kg. The
# officer is given the distance to the target as an input and must provide an
# angle to point the nuzzle of the cannon in -- in radians, from 0 to π/4.

# (These are the only angles that make sense. π/4 to π/2 will duplicate the
# range from 0 to π/4, with higher trajectories, and anything else is either
# shooting backwards or pointing down into the earth or both.)

# We start with a naive officer that just knows that higher trajectories go
# farther, and through training hope that it can reduce its error rate
# significantly

V = 100
G = 9.81
M = 5

ITERATIONS = 1
SAMPLES = 5

# Maximum distance this projectile can go; about 1km
MAX_DISTANCE = Math.round((V*V)/G)

main = () ->

  initial =
    inputs:
      x:
        veryNear: [0, MAX_DISTANCE/4]
        near: [0, MAX_DISTANCE/4, MAX_DISTANCE/2]
        middle: [MAX_DISTANCE/4, MAX_DISTANCE/2, 3*MAX_DISTANCE/2]
        far: [MAX_DISTANCE/2, 3*MAX_DISTANCE/4, MAX_DISTANCE]
        veryFar: [3*MAX_DISTANCE/4, MAX_DISTANCE]
    outputs:
      y:
        veryLow: [0, Math.PI/16]
        low: [0, Math.PI/16, Math.PI/8]
        medium: [Math.PI/16, Math.PI/8, 3*Math.PI/16]
        high: [Math.PI/8, 3*Math.PI/16, Math.PI/4]
        veryHigh: [3*Math.PI/16, Math.PI/4]
    rules: [
      "x INCREASES y WITH 0.5"
    ]

  func = (x) ->
    Math.asin((G*x)/(V*V))/2

  trainToFunction initial, func, 0, MAX_DISTANCE, ITERATIONS, SAMPLES

main()
